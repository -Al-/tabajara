create table tab_funcionarios(
	funMatricula varchar(30) primary key,
	funNome varchar(50),
	funCPF varchar(14),
	funSexo bool,
	funAtivo bool
);
create table tab_telefones(
	telID serial primary key,
	telNumero varchar(11),
	fk_funMatricula varchar(30),
	foreign key (fk_funMatricula) references tab_funcionarios(funMatricula)
);
create table tab_grpAcesso(
	grpID serial primary key,
	grpDescricao varchar(50),
	grpAtivo bool
);
create table tab_usuarios(
	usuID serial primary key,
	usuNome varchar(20),
	usuSenha varchar(20),
	usuAtivo bool,
	fk_funMatricula varchar(30),
	fk_grpID int,
	foreign key (fk_funMatricula) references tab_funcionarios(funMatricula),
	foreign key (fk_grpID) references tab_grpAcesso(grpID)
);
create table tab_sensor(
	senID serial primary key,
	senDescricao varchar(50),
	senParametro varchar(50),
	senAtualizacao int,
	senAtivo bool
);
create table tab_horaRepeticao(
	horID serial primary key,
	horHora varchar(5),
	horDiaInicial date,
	horRepetiraCada int
);
create table tab_arduino(
	ardID serial primary key,
	ardLocal varchar(50),
	ardStatus varchar(100),
	ardAtivo bool
);
create table tab_controle(
	conID serial primary key,
	conCriacao date DEFAULT CURRENT_DATE
);
create table tab_arduino_sensor(
	ardsenID serial primary key,
	fk_ardID int,
	fk_senID int,
	foreign key(fk_ardID) references tab_arduino(ardID),
	foreign key(fk_senID) references tab_sensor(senID)
);
create table tab_sensor_horaRepeticao(
	senhorID serial primary key,
	fk_senID int,
	fk_horID int,
	foreign key(fk_horID) references tab_horaRepeticao(horID),
	foreign key(fk_senID) references tab_sensor(senID)
);
create table tab_arduino_controle(
	ardcontID serial primary key,
	fk_ardID int,
	fk_conID int,
	foreign key(fk_ardID) references tab_arduino(ardID),
	foreign key(fk_conID) references tab_controle(conID)
);
create table tab_controle_grpacesso(
	congrpID serial primary key,
	fk_conID int,
	fk_grpID int,
	foreign key(fk_conID) references tab_controle(conID),
	foreign key(fk_grpID) references tab_grpAcesso(grpID)
);