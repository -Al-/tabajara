package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import model.LedOperationsEnum;
import control.Arduino;

/**
 * @author Danyllo
 */
public class LedArduinoCliente extends JFrame implements ActionListener {

	private static final long serialVersionUID = -5313060376526219954L;
	
	private JButton btLigar;
	private JButton btLigars;
	private JButton btDesligar;
	private JButton btFechar;
	
	public LedArduinoCliente(){
		super();
		getContentPane().setLayout(null);
		
		this.setSize(600, 500);
		this.setLocation(300, 80);
		this.getContentPane().setBackground(Color.WHITE);
			
		this.btLigar = new JButton("Func1");
		this.btLigar.setBounds(10, 50, 150, 30);
		this.btLigar.addActionListener(this);
		this.add(btLigar);
		
		this.btLigars = new JButton("Func4");
		this.btLigars.setBounds(10, 100, 450, 30);
		this.btLigars.addActionListener(this);
		this.add(btLigars);
		
		this.btDesligar = new JButton("Func2");
		this.btDesligar.setBounds(10, 150, 150, 30);
		this.btDesligar.addActionListener(this);
		this.add(btDesligar);
		
		this.btFechar = new JButton("Func3");
		this.btFechar.setBounds(10, 250, 150, 30);
		this.btFechar.addActionListener(this);
		this.add(btFechar);
		
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent source) {
		if(source.getSource() == this.btLigar){
			Arduino conn = Arduino.getSingleton();
			conn.comunicacaoArduino(LedOperationsEnum.FUNC1);
		}
		
		if(source.getSource() == this.btLigars){
			Arduino conn = Arduino.getSingleton();
			conn.comunicacaoArduino(LedOperationsEnum.FUNC4);
		}

		if(source.getSource() == this.btDesligar){
			Arduino conn = Arduino.getSingleton();
			conn.comunicacaoArduino(LedOperationsEnum.FUNC2);
		}

		if(source.getSource() == this.btFechar){
			Arduino conn = Arduino.getSingleton();
			conn.comunicacaoArduino(LedOperationsEnum.FUNC3);
		}
	}

	
	public static void main(String[] args) {
		new LedArduinoCliente();
	}

}
