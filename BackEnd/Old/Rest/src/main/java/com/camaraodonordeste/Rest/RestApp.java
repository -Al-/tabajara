package com.camaraodonordeste.Rest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import modelo.dominio.Usuario;

public class RestApp extends AbstractVerticle {

<<<<<<< HEAD:BackEnd/Rest/src/main/java/com/camaraodonordeste/Rest/RestApp.java
	private static Map<String, Usuario> usuarios;
=======
>>>>>>> 9dad6a55d151d75fa27ad08ca182b804a8a138a1:Rest/src/main/java/com/camaraodonordeste/Rest/RestApp.java

	@Override
	public void start() {
		Router rota = Router.router(vertx);

		rota.route().handler(BodyHandler.create());

		rota.get("/usuarios").handler(this::todos);
		rota.get("/usuarios/:id").handler(this::busca);
		rota.post("/usuarios").handler(this::novo);
		rota.put("/usuarios/:id").handler(this::atualiza);
		rota.delete("/usuarios/:id").handler(this::remove);

		Vertx.vertx().createHttpServer().requestHandler(rota::accept).listen(8080);
	}

	private void todos(RoutingContext contexto) {
		contexto.response().setStatusCode(200).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encode(usuarios));
	}

	private void busca(RoutingContext contexto) {
		String id = contexto.request().getParam("id");
		int cod_retorno = 400;
		String resposta = "Usuario n�o encontrado";
		Usuario busca = usuarios.get(id);
		if (busca != null) {
			cod_retorno = 200;

		}
		contexto.response().setStatusCode(cod_retorno).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encode(busca != null ? busca : new JsonObject().put("resposta", resposta)));
	}

	private void novo(RoutingContext contexto) {
		String boy = contexto.getBodyAsString();

		int cod_retorno = 400;
		String resposta = "Falha ao criar o usuario";
		Usuario busca = Json.decodeValue(boy, Usuario.class);

		if (busca != null) {
			usuarios.put(UUID.randomUUID().toString(), busca);
			cod_retorno = 201;

		}
		contexto.response().setStatusCode(cod_retorno).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encode(busca != null ? new JsonObject().put("resposta", "Usuario criado")
						: new JsonObject().put("resposta", resposta)));
	}

	private void atualiza(RoutingContext contexto) {
		String boy = contexto.getBodyAsString();
		String id = contexto.request().getParam("id");
		int cod_retorno = 400;
		String resposta = "N�o foi possivel atualizar o usuario";
		Usuario busca = Json.decodeValue(boy, Usuario.class);

		if (busca != null) {
			usuarios.put(id, busca);
			cod_retorno = 200;
			resposta = "Usuario Atualizado";

		}
		contexto.response().setStatusCode(cod_retorno).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encode(busca != null ? new JsonObject().put("resposta", resposta)
						: new JsonObject().put("resposta", resposta)));
	}

	private void remove(RoutingContext contexto) {
		String id = contexto.request().getParam("id");
		int cod_retorno = 400;
		String resposta = "Usuario n�o encontrado";
		Usuario busca = usuarios.remove(id);

		if (busca != null) {
			cod_retorno = 200;
			resposta = "Usuario apagado";

		}
		contexto.response().setStatusCode(cod_retorno).putHeader("content-type", "application/json; charset=utf-8")
				.end(Json.encode(busca != null ? new JsonObject().put("resposta", resposta)
						: new JsonObject().put("resposta", resposta)));
	}

	public static void main(String[] arg) {
		new RestApp().start();

	}
}
