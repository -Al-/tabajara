package modelo.dominio;

import java.io.Serializable;

public class Usuario implements Serializable{
	private String nome;
	private String sobre_nome;
	private String endereco;
	
	public Usuario() {
		super();
	}

	public Usuario(String nome, String sobre_nome, String endereco) {
		super();
		this.nome = nome;
		this.sobre_nome = sobre_nome;
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobre_nome() {
		return sobre_nome;
	}

	public void setSobre_nome(String sobre_nome) {
		this.sobre_nome = sobre_nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sobre_nome == null) ? 0 : sobre_nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sobre_nome == null) {
			if (other.sobre_nome != null)
				return false;
		} else if (!sobre_nome.equals(other.sobre_nome))
			return false;
		return true;
	}
		
}
