package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

public class Arduino {
	private Integer ardID;
	private String ardLocal;
	private String ardStatus;
	private boolean ardAtivo;
	private boolean ardVirtual;
	private Tanque tanque;
	private ArrayList<Controle> controles;
	private ArrayList<Sensor> sensores;

	public Arduino(Integer ardID, String ardLocal, String ardStatus, boolean ardAtivo, boolean ardVirtual,
			Tanque tanque, ArrayList<Controle> controles, ArrayList<Sensor> sensores) {
		super();
		this.ardID = ardID;
		this.ardLocal = ardLocal;
		this.ardStatus = ardStatus;
		this.ardAtivo = ardAtivo;
		this.tanque = tanque;
		this.controles = controles;
		this.sensores = sensores;
		this.ardVirtual = ardVirtual;
	}

	public Arduino() {
		super();
		this.controles = new ArrayList<Controle>();
		this.sensores = new ArrayList<Sensor>();
	}

	public boolean addItem(Sensor sen) {
		if (!sensores.contains(sen)) {
			sensores.add(sen);
			return true;
		}
		return false;
	}

	public boolean remItem(Sensor sen) {
		if (sensores.contains(sen)) {
			sensores.remove(sen);
			return true;
		}
		return false;
	}

	public boolean addItem(Controle con) {
		if (!controles.contains(con)) {
			controles.add(con);
			return true;
		}
		return false;
	}

	public boolean remItem(Controle con) {
		if (controles.contains(con)) {
			controles.remove(con);
			return true;
		}
		return false;
	}

	public boolean isArdVirtual() {
		return ardVirtual;
	}

	public void setArdVirtual(boolean ardVirtual) {
		this.ardVirtual = ardVirtual;
	}

	public Integer getArdID() {
		return ardID;
	}

	public void setArdID(Integer ardID) {
		this.ardID = ardID;
	}

	public String getArdLocal() {
		return ardLocal;
	}

	public void setArdLocal(String ardLocal) {
		this.ardLocal = ardLocal;
	}

	public String getArdStatus() {
		return ardStatus;
	}

	public void setArdStatus(String ardStatus) {
		this.ardStatus = ardStatus;
	}

	public boolean isArdAtivo() {
		return ardAtivo;
	}

	public void setArdAtivo(boolean ardAtivo) {
		this.ardAtivo = ardAtivo;
	}

	public Tanque getTanque() {
		return tanque;
	}

	public void setTanque(Tanque tanque) {
		this.tanque = tanque;
	}

	public ArrayList<Controle> getControles() {
		return controles;
	}

	public void setControles(ArrayList<Controle> controles) {
		this.controles = controles;
	}

	public ArrayList<Sensor> getSensores() {
		return sensores;
	}

	public void setSensores(ArrayList<Sensor> sensores) {
		this.sensores = sensores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ardID == null) ? 0 : ardID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arduino other = (Arduino) obj;
		if (ardID == null) {
			if (other.ardID != null)
				return false;
		} else if (!ardID.equals(other.ardID))
			return false;
		return true;
	}

}
