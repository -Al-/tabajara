package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

public class Tanque {
	private Integer tanId;
	private String tanIdentificacao;
	private String tanDatainicio;
	private ArrayList<Arduino> arduinos;

	public Tanque(Integer tanId, String tanIdentificacao, String tanDatainicio, ArrayList<Arduino> arduinos) {
		super();
		this.tanId = tanId;
		this.tanIdentificacao = tanIdentificacao;
		this.tanDatainicio = tanDatainicio;
		this.arduinos = arduinos;
	}

	public Tanque() {
		super();
	}

	public boolean addItem(Arduino ard) {
		if (!arduinos.contains(ard)) {
			arduinos.add(ard);
			return true;
		}
		return false;
	}

	public boolean remItem(Arduino ard) {
		if (arduinos.contains(ard)) {
			arduinos.remove(ard);
			return true;
		}
		return false;
	}

	public Integer getTanId() {
		return tanId;
	}

	public void setTanId(Integer tanId) {
		this.tanId = tanId;
	}

	public String getTanIdentificacao() {
		return tanIdentificacao;
	}

	public void setTanIdentificacao(String tanIdentificacao) {
		this.tanIdentificacao = tanIdentificacao;
	}

	public String getTanDatainicio() {
		return tanDatainicio;
	}

	public void setTanDatainicio(String tanDatainicio) {
		this.tanDatainicio = tanDatainicio;
	}

	public ArrayList<Arduino> getArduinos() {
		return arduinos;
	}

	public void setArduinos(ArrayList<Arduino> arduinos) {
		this.arduinos = arduinos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tanId == null) ? 0 : tanId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tanque other = (Tanque) obj;
		if (tanId == null) {
			if (other.tanId != null)
				return false;
		} else if (!tanId.equals(other.tanId))
			return false;
		return true;
	}

}
