package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

import com.tabajara.backend.modelo.enums.TipoSensor;

public class Sensor {
	private Integer senID;
	private String senDescricao;
	private String senParametro;
	private Integer senAtualizacao;
	private boolean senAtivo;
	private boolean senVirtual;
	private TipoSensor tipoSensor;
	private ArrayList<HoraRepeticao> horaRepeticao;

	public Sensor(Integer senID, String senDescricao, String senParametro, Integer senAtualizacao, boolean senAtivo,
			boolean senVirtual, TipoSensor tipoSensor, ArrayList<HoraRepeticao> horaRepeticao) {
		super();
		this.senID = senID;
		this.senDescricao = senDescricao;
		this.senParametro = senParametro;
		this.senAtualizacao = senAtualizacao;
		this.senAtivo = senAtivo;
		this.senVirtual = senVirtual;
		this.tipoSensor = tipoSensor;
		this.horaRepeticao = horaRepeticao;
	}

	public Sensor() {
		super();
	}

	public boolean addItem(HoraRepeticao hora) {
		if (!horaRepeticao.contains(hora)) {
			horaRepeticao.add(hora);
			return true;
		}
		return false;
	}

	public boolean remItem(HoraRepeticao hora) {
		if (horaRepeticao.contains(hora)) {
			horaRepeticao.remove(hora);
			return true;
		}
		return false;
	}

	public Integer getSenID() {
		return senID;
	}

	public void setSenID(Integer senID) {
		this.senID = senID;
	}

	public String getSenDescricao() {
		return senDescricao;
	}

	public void setSenDescricao(String senDescricao) {
		this.senDescricao = senDescricao;
	}

	public String getSenParametro() {
		return senParametro;
	}

	public void setSenParametro(String senParametro) {
		this.senParametro = senParametro;
	}

	public Integer getSenAtualizacao() {
		return senAtualizacao;
	}

	public void setSenAtualizacao(Integer senAtualizacao) {
		this.senAtualizacao = senAtualizacao;
	}

	public boolean isSenAtivo() {
		return senAtivo;
	}

	public void setSenAtivo(boolean senAtivo) {
		this.senAtivo = senAtivo;
	}

	public boolean isSenVirtual() {
		return senVirtual;
	}

	public void setSenVirtual(boolean senVirtual) {
		this.senVirtual = senVirtual;
	}

	public TipoSensor getTipoSensor() {
		return tipoSensor;
	}

	public void setTipoSensor(TipoSensor tipoSensor) {
		this.tipoSensor = tipoSensor;
	}

	public ArrayList<HoraRepeticao> getHoraRepeticao() {
		return horaRepeticao;
	}

	public void setHoraRepeticao(ArrayList<HoraRepeticao> horaRepeticao) {
		this.horaRepeticao = horaRepeticao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((senID == null) ? 0 : senID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sensor other = (Sensor) obj;
		if (senID == null) {
			if (other.senID != null)
				return false;
		} else if (!senID.equals(other.senID))
			return false;
		return true;
	}

}
