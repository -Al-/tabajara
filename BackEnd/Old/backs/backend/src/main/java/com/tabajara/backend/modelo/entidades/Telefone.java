package com.tabajara.backend.modelo.entidades;

public class Telefone {

	private Integer telID;
	private String telNumero;
	private Funcionario func;

	public Telefone(Integer telID, String telNumero, Funcionario func) {
		super();
		this.telID = telID;
		this.telNumero = telNumero;
		this.func = func;
	}

	public Telefone() {
		super();
	}

	public Integer getTelID() {
		return telID;
	}

	public void setTelID(Integer telID) {
		this.telID = telID;
	}

	public String getTelNumero() {
		return telNumero;
	}

	public void setTelNumero(String telNumero) {
		this.telNumero = telNumero;
	}

	public Funcionario getFunc() {
		return func;
	}

	public void setFunc(Funcionario func) {
		this.func = func;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telID == null) ? 0 : telID.hashCode());
		result = prime * result + ((telNumero == null) ? 0 : telNumero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefone other = (Telefone) obj;
		if (telID == null) {
			if (other.telID != null)
				return false;
		} else if (!telID.equals(other.telID))
			return false;
		if (telNumero == null) {
			if (other.telNumero != null)
				return false;
		} else if (!telNumero.equals(other.telNumero))
			return false;
		return true;
	}

}
