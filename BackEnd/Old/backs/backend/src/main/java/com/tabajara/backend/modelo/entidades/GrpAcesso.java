package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

public class GrpAcesso {
	private Integer grpID;
	private String grpDescricao;
	private boolean grpAtivo;
	private ArrayList<Funcionario> funcionarios;
	private ArrayList<Controle> controles;

	public GrpAcesso(Integer grpID, String grpDescricao, boolean grpAtivo, ArrayList<Funcionario> funcionarios,
			ArrayList<Controle> controles) {
		super();
		this.grpID = grpID;
		this.grpDescricao = grpDescricao;
		this.grpAtivo = grpAtivo;
		this.funcionarios = funcionarios;
		this.controles = controles;
	}

	public GrpAcesso() {
		super();
	}

	public boolean addItem(Controle con) {
		if (!controles.contains(con)) {
			controles.add(con);
			return true;
		}
		return false;
	}

	public boolean remItem(Controle con) {
		if (controles.contains(con)) {
			controles.remove(con);
			return true;
		}
		return false;
	}

	public boolean addItem(Funcionario fun) {
		if (!funcionarios.contains(fun)) {
			funcionarios.add(fun);
			return true;
		}
		return false;
	}

	public boolean remItem(Funcionario fun) {
		if (funcionarios.contains(fun)) {
			funcionarios.remove(fun);
			return true;
		}
		return false;
	}

	public Integer getGrpID() {
		return grpID;
	}

	public void setGrpID(Integer grpID) {
		this.grpID = grpID;
	}

	public String getGrpDescricao() {
		return grpDescricao;
	}

	public void setGrpDescricao(String grpDescricao) {
		this.grpDescricao = grpDescricao;
	}

	public boolean isGrpAtivo() {
		return grpAtivo;
	}

	public void setGrpAtivo(boolean grpAtivo) {
		this.grpAtivo = grpAtivo;
	}

	public ArrayList<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(ArrayList<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public ArrayList<Controle> getControles() {
		return controles;
	}

	public void setControles(ArrayList<Controle> controles) {
		this.controles = controles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grpID == null) ? 0 : grpID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrpAcesso other = (GrpAcesso) obj;
		if (grpID == null) {
			if (other.grpID != null)
				return false;
		} else if (!grpID.equals(other.grpID))
			return false;
		return true;
	}

}
