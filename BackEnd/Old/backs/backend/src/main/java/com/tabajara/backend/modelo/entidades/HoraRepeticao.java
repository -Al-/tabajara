package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

import com.tabajara.backend.modelo.enums.TipoRepeticao;

public class HoraRepeticao {
	private Integer horID;
	private String horHora;
	private String horDiaInicial;
	private Integer horRepetiraCada;
	private TipoRepeticao tipoRepeticao;
	private ArrayList<Sensor> sensores;

	public HoraRepeticao(Integer horID, String horHora, String horDiaInicial, Integer horRepetiraCada,
			TipoRepeticao tipoRepeticao, ArrayList<Sensor> sensores) {
		super();
		this.horID = horID;
		this.horHora = horHora;
		this.horDiaInicial = horDiaInicial;
		this.horRepetiraCada = horRepetiraCada;
		this.tipoRepeticao = tipoRepeticao;
		this.sensores = sensores;
	}

	public HoraRepeticao() {
		super();
	}

	public boolean addItem(Sensor sen) {
		if (!sensores.contains(sen)) {
			sensores.add(sen);
			return true;
		}
		return false;
	}

	public boolean remItem(Sensor sen) {
		if (sensores.contains(sen)) {
			sensores.remove(sen);
			return true;
		}
		return false;
	}

	public Integer getHorID() {
		return horID;
	}

	public void setHorID(Integer horID) {
		this.horID = horID;
	}

	public String getHorHora() {
		return horHora;
	}

	public void setHorHora(String horHora) {
		this.horHora = horHora;
	}

	public String getHorDiaInicial() {
		return horDiaInicial;
	}

	public void setHorDiaInicial(String horDiaInicial) {
		this.horDiaInicial = horDiaInicial;
	}

	public Integer getHorRepetiraCada() {
		return horRepetiraCada;
	}

	public void setHorRepetiraCada(Integer horRepetiraCada) {
		this.horRepetiraCada = horRepetiraCada;
	}

	public TipoRepeticao getTipoRepeticao() {
		return tipoRepeticao;
	}

	public void setTipoRepeticao(TipoRepeticao tipoRepeticao) {
		this.tipoRepeticao = tipoRepeticao;
	}

	public ArrayList<Sensor> getSensores() {
		return sensores;
	}

	public void setSensores(ArrayList<Sensor> sensores) {
		this.sensores = sensores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((horID == null) ? 0 : horID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HoraRepeticao other = (HoraRepeticao) obj;
		if (horID == null) {
			if (other.horID != null)
				return false;
		} else if (!horID.equals(other.horID))
			return false;
		return true;
	}

}
