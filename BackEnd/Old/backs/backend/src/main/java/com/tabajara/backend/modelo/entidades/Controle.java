package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

public class Controle {
	private Integer conID;
	private String conCriacao;
	private ArrayList<Arduino> arduinos;
	private ArrayList<GrpAcesso> grupos;

	public Controle(Integer conID, String conCriacao, ArrayList<Arduino> arduinos, ArrayList<GrpAcesso> grupos) {
		super();
		this.conID = conID;
		this.conCriacao = conCriacao;
		this.arduinos = arduinos;
		this.grupos = grupos;
	}

	public boolean addItem(Arduino ard) {
		if (!arduinos.contains(ard)) {
			arduinos.add(ard);
			return true;
		}
		return false;
	}

	public boolean remItem(Arduino ard) {
		if (arduinos.contains(ard)) {
			arduinos.remove(ard);
			return true;
		}
		return false;
	}

	public boolean addItem(GrpAcesso grp) {
		if (!grupos.contains(grp)) {
			grupos.add(grp);
			return true;
		}
		return false;
	}

	public boolean remItem(GrpAcesso grp) {
		if (grupos.contains(grp)) {
			grupos.remove(grp);
			return true;
		}
		return false;
	}

	public Controle() {
		super();
	}

	public Integer getConID() {
		return conID;
	}

	public void setConID(Integer conID) {
		this.conID = conID;
	}

	public String getConCriacao() {
		return conCriacao;
	}

	public void setConCriacao(String conCriacao) {
		this.conCriacao = conCriacao;
	}

	public ArrayList<Arduino> getArduinos() {
		return arduinos;
	}

	public void setArduinos(ArrayList<Arduino> arduinos) {
		this.arduinos = arduinos;
	}

	public ArrayList<GrpAcesso> getGrupos() {
		return grupos;
	}

	public void setGrupos(ArrayList<GrpAcesso> grupos) {
		this.grupos = grupos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conID == null) ? 0 : conID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Controle other = (Controle) obj;
		if (conID == null) {
			if (other.conID != null)
				return false;
		} else if (!conID.equals(other.conID))
			return false;
		return true;
	}

}
