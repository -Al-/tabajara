package com.tabajara.backend.modelo.entidades;

import java.util.ArrayList;

public class Funcionario {

	private String funMatricula;
	private String funNome;
	private String funCPF;
	private boolean funSexo;
	private boolean funAtivo;
	private GrpAcesso grupo;
	private ArrayList<Telefone> tel;

	public Funcionario(String funMatricula, String funNome, String funCPF, boolean funSexo, boolean funAtivo,
			GrpAcesso grupo, ArrayList<Telefone> tel) {
		super();
		this.funMatricula = funMatricula;
		this.funNome = funNome;
		this.funCPF = funCPF;
		this.funSexo = funSexo;
		this.funAtivo = funAtivo;
		this.tel = tel;
		this.grupo = grupo;
	}

	public boolean addItem(Telefone _tel) {
		if (!tel.contains(_tel)) {
			tel.add(_tel);
			return true;
		}
		return false;
	}

	public boolean remItem(Telefone _tel) {
		if (tel.contains(_tel)) {
			tel.remove(_tel);
			return true;
		}
		return false;
	}

	public Funcionario() {
		super();
	}

	public GrpAcesso getGrupo() {
		return grupo;
	}

	public void setGrupo(GrpAcesso grupo) {
		this.grupo = grupo;
	}

	public String getFunMatricula() {
		return funMatricula;
	}

	public void setFunMatricula(String funMatricula) {
		this.funMatricula = funMatricula;
	}

	public String getFunNome() {
		return funNome;
	}

	public void setFunNome(String funNome) {
		this.funNome = funNome;
	}

	public String getFunCPF() {
		return funCPF;
	}

	public void setFunCPF(String funCPF) {
		this.funCPF = funCPF;
	}

	public boolean isFunSexo() {
		return funSexo;
	}

	public void setFunSexo(boolean funSexo) {
		this.funSexo = funSexo;
	}

	public boolean isFunAtivo() {
		return funAtivo;
	}

	public void setFunAtivo(boolean funAtivo) {
		this.funAtivo = funAtivo;
	}

	public ArrayList<Telefone> getTel() {
		return tel;
	}

	public void setTel(ArrayList<Telefone> tel) {
		this.tel = tel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((funCPF == null) ? 0 : funCPF.hashCode());
		result = prime * result + ((funMatricula == null) ? 0 : funMatricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (funCPF == null) {
			if (other.funCPF != null)
				return false;
		} else if (!funCPF.equals(other.funCPF))
			return false;
		if (funMatricula == null) {
			if (other.funMatricula != null)
				return false;
		} else if (!funMatricula.equals(other.funMatricula))
			return false;
		return true;
	}

}
