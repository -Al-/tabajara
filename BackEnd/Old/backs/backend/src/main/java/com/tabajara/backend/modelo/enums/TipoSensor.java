package com.tabajara.backend.modelo.enums;

public enum TipoSensor {
	SALINIDADE,
	TEMPERATURA,
	TRANSPARÊNCIA,
	MATERIAORGANICA,
	OXIGÊNIO,
	PH,
	ALCALINIDADE,
	DUREZA,
	AMONIA,
	NITRITO,
	NITRATO,
	H2S,
	SILICATO
}
