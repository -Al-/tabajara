package com.tabajara.backend.modelo.entidades;

public class Usuario {

	private Integer usuID;
	private String usuNome;
	private String usuSenha;
	private boolean usuAtivo;
	private Funcionario funcionario;
	private GrpAcesso grupo;

	public Usuario(Integer usuID, String usuNome, String usuSenha, boolean usuAtivo, Funcionario funcionario,
			GrpAcesso grupo) {
		super();
		this.usuID = usuID;
		this.usuNome = usuNome;
		this.usuSenha = usuSenha;
		this.usuAtivo = usuAtivo;
		this.funcionario = funcionario;
		this.grupo = grupo;
	}

	public Usuario() {
		super();
	}

	public Integer getUsuID() {
		return usuID;
	}

	public void setUsuID(Integer usuID) {
		this.usuID = usuID;
	}

	public String getUsuNome() {
		return usuNome;
	}

	public void setUsuNome(String usuNome) {
		this.usuNome = usuNome;
	}

	public String getUsuSenha() {
		return usuSenha;
	}

	public void setUsuSenha(String usuSenha) {
		this.usuSenha = usuSenha;
	}

	public boolean isUsuAtivo() {
		return usuAtivo;
	}

	public void setUsuAtivo(boolean usuAtivo) {
		this.usuAtivo = usuAtivo;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public GrpAcesso getGrupo() {
		return grupo;
	}

	public void setGrupo(GrpAcesso grupo) {
		this.grupo = grupo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((usuID == null) ? 0 : usuID.hashCode());
		result = prime * result + ((usuNome == null) ? 0 : usuNome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (usuID == null) {
			if (other.usuID != null)
				return false;
		} else if (!usuID.equals(other.usuID))
			return false;
		if (usuNome == null) {
			if (other.usuNome != null)
				return false;
		} else if (!usuNome.equals(other.usuNome))
			return false;
		return true;
	}

}
