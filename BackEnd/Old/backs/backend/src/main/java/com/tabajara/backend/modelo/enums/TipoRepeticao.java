package com.tabajara.backend.modelo.enums;

public enum TipoRepeticao {
	DIARIAMENTE,
	SEMANALMENTE,
	MENSALMENTE,
	UMAVEZ
}
