buildscript {
    ext {
        vertx_jooq_version = '3.0.2'
        postgresql_version = '42.1.4'
    }
    repositories {
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        classpath "io.github.jklingsporn:vertx-jooq-generate:$vertx_jooq_version"
        classpath "org.postgresql:postgresql:$postgresql_version"
    }
}

plugins {
  id 'java'
  id 'application'
  id 'com.github.johnrengelman.shadow' version '2.0.1'
}

repositories {
  jcenter()
  maven {
    url "https://oss.sonatype.org/content/repositories/iovertx-3750/"
  }
}

def jooq='3.11.0'
def vertx='3.5.2.CR3'
mainClassName = 'io.vertx.core.Launcher'
def mainVerticleName = 'com.tabajara.MainVerticle'
def watchForChange = 'src/**/*.java'
def doOnChange

dependencies {
    compile "org.jooq:jooq:$jooq"    
	compile "io.github.jklingsporn:vertx-jooq-rx-jdbc:3.1.0"    
    compile "io.vertx:vertx-web:$vertx"
    compile "io.vertx:vertx-core:$vertx"
    
    
    testImplementation 'junit:junit:4.12'
}

if (System.getProperty("os.name").toLowerCase().contains("windows")) {
  doOnChange = '.\\gradlew classes'
} else {
  doOnChange = './gradlew classes'
}

run {
  args = ['run', mainVerticleName, "--redeploy=$watchForChange", "--launcher-class=$mainClassName", "--on-redeploy=$doOnChange"]
}

shadowJar {
  classifier = 'fat'
  manifest {
    attributes "Main-Verticle": mainVerticleName
  }
  mergeServiceFiles {
    include 'META-INF/services/io.vertx.core.spi.VerticleFactory'
  }
}

compileJava {
  targetCompatibility = 1.8
  sourceCompatibility = 1.8
}

