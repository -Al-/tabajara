package com.tabajara;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;

public class MainVerticle extends AbstractVerticle {
	private HttpServer server;

	@Override
	public void start(Future<Void> future) {

		server = createServer();
		server.requestHandler(new MainVerticleAPI(vertx, config()));
		server.listen(res -> {
			if (res.failed()) {
				future.fail(res.cause());
			} else {
				future.complete();
			}
		});
	}

	@Override
	public void stop(Future<Void> future) {
		if (server == null) {
			future.complete();
			return;
		}
		server.close(future.completer());
	}

	private HttpServer createServer() {
		HttpServerOptions options = new HttpServerOptions();
		options.setHost(config.getDBHost());
		options.setPort(config.getHostPort());
		return vertx.createHttpServer(options);
	}

}
