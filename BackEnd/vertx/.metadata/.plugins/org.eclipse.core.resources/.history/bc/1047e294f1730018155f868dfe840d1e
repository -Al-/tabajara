package com.tabajara;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static io.vertx.core.http.HttpMethod.DELETE;
import static io.vertx.core.http.HttpMethod.GET;
import static io.vertx.core.http.HttpMethod.OPTIONS;
import static io.vertx.core.http.HttpMethod.PATCH;
import static io.vertx.core.http.HttpMethod.POST;
import static io.vertx.core.http.HttpMethod.PUT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import org.jooq.Configuration;
import org.jooq.SQLDialect;

import com.tabajara.base.tables.Arduino;
import com.tabajara.base.tables.daos.ArduinoDao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class MainVerticleAPI implements Handler<HttpServerRequest> {

	private final Router router;
	private final Vertx vertx;
	private String Host, DBHost, DBUser, DBPassword, DBBase, DBUrl, DBTipo;
	private Configuration conf;
	private Integer HostPort, DBPort;
	// private final TodoDAO dao;

	public MainVerticleAPI(Vertx vertx, JsonObject config) {
		// dao = TodoDAO.create(vertx, MAP);
		this.vertx = vertx;
		this.Host = config.getString("http.address", "127.0.0.1");
		this.HostPort = config.getInteger("http.port", 8088);
		this.DBHost = config.getString("db.address", "127.0.0.1");
		this.DBPort = config.getInteger("db.porta", 5432);
		this.DBUser = config.getString("db.user", "postgres");
		this.DBPassword = config.getString("db.password", "postgres");
		this.DBBase = config.getString("db.DBBase", "tabajara");
		this.DBTipo = config.getString("db.Tipo", "postgresql");
		this.DBUrl = config.getString("db.DBUrl", "jdbc:" + DBTipo + "://" + DBHost + ":" + DBPort + "/" + DBBase);
		if (DBTipo.toUpperCase() == "POSTGRESQL") {
			conf.set(SQLDialect.POSTGRES);
		} else {
			conf.set(SQLDialect.MYSQL);
		}
		router = Router.router(vertx);
		attachRoutes();
	}

	@Override
	public void handle(HttpServerRequest request) {
		router.accept(request);
	}

	private void attachRoutes() {
		CorsHandler cors = CorsHandler.create("*");
		cors.allowedHeader(CONTENT_TYPE.toString());
		cors.allowedMethods(new HashSet<>(Arrays.asList(GET, POST, PUT, PATCH, DELETE, OPTIONS)));
		router.route().handler(cors);
		router.route().handler(BodyHandler.create());
		// router.route("/").handler("content-type", "text/html").end("<h1>Super
		// Camar�o</h1><p><h3>Servidor Rest Online");

		router.get(Constantes.ARD_GET).handler(this::handleGetARD);
		router.get(Constantes.ARD_LIST_ALL).handler(this::handleGetAllARD);
		router.post(Constantes.ARD_CREATE).handler(this::handleCreateARD);
		// router.patch(Constantes.ARD_UPDATE).handler(this::handleUpdateARD);
		// router.delete(Constantes.ARD_DELETE).handler(this::handleDeleteOneARD);
		// router.delete(Constantes.ARD_DELETE_ALL).handler(this::handleDeleteAllARD);

		// router.get("/").handler(ctx -> {
		// ctx.response().end(new JsonArray(dao.getTodos()).toString());
		// });
		//
		// router.delete("/").handler(ctx -> {
		// dao.clearTodos();
		// ctx.response().end();
		// });
		//
		// router.post("/").handler(this::checkRequestBody);
		// router.post("/").handler(ctx -> {
		// JsonObject todo = dao.newTodo(ctx.getBodyAsJson());
		// ctx.put(PAYLOAD, todo);
		// ctx.next();
		// });
		// router.post("/").handler(this::sendPayload);
		//
		//
		// router.get("/todos/:id").handler(this::checkId);
		// router.get("/todos/:id").handler(this::sendPayload);
		//
		// router.put("/todos/:id").handler(this::checkId);
		// router.put("/todos/:id").handler(this::checkRequestBody);
		// router.put("/todos/:id").handler(this::updateTodo);
		// router.put("/todos/:id").handler(this::sendPayload);
		//
		// router.patch("/todos/:id").handler(this::checkId);
		// router.patch("/todos/:id").handler(this::checkRequestBody);
		// router.patch("/todos/:id").handler(this::updateTodo);
		// router.patch("/todos/:id").handler(this::sendPayload);
		//
		// router.delete("/todos/:id").handler(this::checkId);
		// router.delete("/todos/:id").handler(ctx -> {
		// dao.deleteTodo(ctx.get(ID_PARAM));
		// ctx.response().end();
		// });
	}

	public JsonObject getConfigJson() {
		JsonObject conf = new JsonObject();
		conf.put("user", DBUser);
		conf.put("password", "postgres");
		conf.put("port", DBPort);
		conf.put("url", DBUrl);
		conf.put("host", DBHost);
		return conf;
	}

	private void handleGetAllARD(RoutingContext context) {
		ArduinoDao dao = new ArduinoDao(conf.set(getConn(context)), vertx);
		sendResponse(context, dao.findAll().result().toArray());

	}

	private void handleCreateARD(RoutingContext context) {
		ArduinoDao dao = new ArduinoDao(config.getConfig().set(getConn(context)), vertx);
		try {
			JsonObject rawEntity = context.getBodyAsJson();
			if (!Objects.isNull(rawEntity)) {
				com.tabajara.base.tables.pojos.Arduino ard = new com.tabajara.base.tables.pojos.Arduino(rawEntity);
				sendResponse(context, dao.insert(ard));
				return;
			}
			badRequest(context);
		} catch (DecodeException ex) {
			badRequest(context, ex);
		}
	}

	public Connection getConn(RoutingContext context) {
		try {

			connection = DriverManager.getConnection(config.getDBUrl(), config.getDBUrl(), config.getDBPassword());

		} catch (Exception e) {
			badRequest(context, e);
		}
		return connection;
	}

	private void handleGetARD(RoutingContext context) {
		Arduino ard = null;
		try {
			Integer id = Integer.parseInt(context.request().getParam("id"));
			ArduinoDao dao = new ArduinoDao(config.getConfig().set(getConn(context)), vertx);
			sendResponse(context, dao.findOneById(id));
			return;
		} catch (Exception e) {
			badRequest(context);
		}
	}

	protected void sendResponse(RoutingContext context, Object[] objects) {
		if (objects == null) {
			notFound(context);
		} else {
			context.response().putHeader("content-type", "application/json;charset=utf-8")
					.end(Json.encodePrettily(objects));
		}
	}

	protected void sendResponse(RoutingContext context, Object objects) {
		if (objects == null) {
			notFound(context);
		} else {
			context.response().putHeader("content-type", "application/json;charset=utf-8")
					.end(Json.encodePrettily(objects));
		}
	}

	public void ok(RoutingContext context) {
		context.response().end();
	}

	public void ok(RoutingContext context, String content) {
		context.response().setStatusCode(200).putHeader("content-type", "application/json").end(content);
	}

	public void created(RoutingContext context) {
		context.response().setStatusCode(201).end();
	}

	public void created(RoutingContext context, String content) {
		context.response().setStatusCode(201).putHeader("content-type", "application/json").end(content);
	}

	public void noContent(RoutingContext context) {
		context.response().setStatusCode(204).end();
	}

	public void badRequest(RoutingContext context, Throwable ex) {
		context.response().setStatusCode(400).putHeader("content-type", "application/json")
				.end(new JsonObject().put("error", ex.getMessage()).encodePrettily());
	}

	public void badRequest(RoutingContext context) {
		context.response().setStatusCode(400).putHeader("content-type", "application/json")
				.end(new JsonObject().put("error", "bad_request").encodePrettily());
	}

	public void notFound(RoutingContext context) {
		context.response().setStatusCode(404).putHeader("content-type", "application/json")
				.end(new JsonObject().put("message", "not_found").encodePrettily());
	}

	public void internalError(RoutingContext context, Throwable ex) {
		context.response().setStatusCode(500).putHeader("content-type", "application/json")
				.end(new JsonObject().put("error", ex.getMessage()).encodePrettily());
	}

	public void internalError(RoutingContext context, String cause) {
		context.response().setStatusCode(500).putHeader("content-type", "application/json")
				.end(new JsonObject().put("error", cause).encodePrettily());
	}

	public void serviceUnavailable(RoutingContext context) {
		context.fail(503);
	}

	public void serviceUnavailable(RoutingContext context, Throwable ex) {
		context.response().setStatusCode(503).putHeader("content-type", "application/json")
				.end(new JsonObject().put("error", ex.getMessage()).encodePrettily());
	}

	public void serviceUnavailable(RoutingContext context, String cause) {
		context.response().setStatusCode(503).putHeader("content-type", "application/json")
				.end(new JsonObject().put("error", cause).encodePrettily());
	}

}
