/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.records;


import com.tabajara.base.tables.Arduino;
import com.tabajara.base.tables.interfaces.IArduino;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ArduinoRecord extends UpdatableRecordImpl<ArduinoRecord> implements VertxPojo, Record6<Integer, String, String, String, String, Integer>, IArduino {

    private static final long serialVersionUID = 46977469;

    /**
     * Setter for <code>public.arduino.ardid</code>.
     */
    @Override
    public ArduinoRecord setArdid(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.arduino.ardid</code>.
     */
    @Override
    public Integer getArdid() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.arduino.ardlocal</code>.
     */
    @Override
    public ArduinoRecord setArdlocal(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.arduino.ardlocal</code>.
     */
    @Override
    public String getArdlocal() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.arduino.ardstatus</code>.
     */
    @Override
    public ArduinoRecord setArdstatus(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.arduino.ardstatus</code>.
     */
    @Override
    public String getArdstatus() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.arduino.ardvirtual</code>.
     */
    @Override
    public ArduinoRecord setArdvirtual(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>public.arduino.ardvirtual</code>.
     */
    @Override
    public String getArdvirtual() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.arduino.ardativo</code>.
     */
    @Override
    public ArduinoRecord setArdativo(String value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>public.arduino.ardativo</code>.
     */
    @Override
    public String getArdativo() {
        return (String) get(4);
    }

    /**
     * Setter for <code>public.arduino.fktanid</code>.
     */
    @Override
    public ArduinoRecord setFktanid(Integer value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>public.arduino.fktanid</code>.
     */
    @Override
    public Integer getFktanid() {
        return (Integer) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, String, Integer> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, String, Integer> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Arduino.ARDUINO.ARDID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Arduino.ARDUINO.ARDLOCAL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Arduino.ARDUINO.ARDSTATUS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Arduino.ARDUINO.ARDVIRTUAL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return Arduino.ARDUINO.ARDATIVO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return Arduino.ARDUINO.FKTANID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getArdid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getArdlocal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getArdstatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getArdvirtual();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getArdativo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component6() {
        return getFktanid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getArdid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getArdlocal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getArdstatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getArdvirtual();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getArdativo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getFktanid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord value1(Integer value) {
        setArdid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord value2(String value) {
        setArdlocal(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord value3(String value) {
        setArdstatus(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord value4(String value) {
        setArdvirtual(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord value5(String value) {
        setArdativo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord value6(Integer value) {
        setFktanid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinoRecord values(Integer value1, String value2, String value3, String value4, String value5, Integer value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(IArduino from) {
        setArdid(from.getArdid());
        setArdlocal(from.getArdlocal());
        setArdstatus(from.getArdstatus());
        setArdvirtual(from.getArdvirtual());
        setArdativo(from.getArdativo());
        setFktanid(from.getFktanid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends IArduino> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ArduinoRecord
     */
    public ArduinoRecord() {
        super(Arduino.ARDUINO);
    }

    /**
     * Create a detached, initialised ArduinoRecord
     */
    public ArduinoRecord(Integer ardid, String ardlocal, String ardstatus, String ardvirtual, String ardativo, Integer fktanid) {
        super(Arduino.ARDUINO);

        set(0, ardid);
        set(1, ardlocal);
        set(2, ardstatus);
        set(3, ardvirtual);
        set(4, ardativo);
        set(5, fktanid);
    }

    public ArduinoRecord(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
