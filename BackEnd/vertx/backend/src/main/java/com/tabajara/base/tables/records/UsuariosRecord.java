/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.records;


import com.tabajara.base.tables.Usuarios;
import com.tabajara.base.tables.interfaces.IUsuarios;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UsuariosRecord extends UpdatableRecordImpl<UsuariosRecord> implements VertxPojo, Record6<Integer, String, String, String, String, Integer>, IUsuarios {

    private static final long serialVersionUID = -1288811738;

    /**
     * Setter for <code>public.usuarios.usuid</code>.
     */
    @Override
    public UsuariosRecord setUsuid(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.usuarios.usuid</code>.
     */
    @Override
    public Integer getUsuid() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.usuarios.usunome</code>.
     */
    @Override
    public UsuariosRecord setUsunome(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.usuarios.usunome</code>.
     */
    @Override
    public String getUsunome() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.usuarios.ususenha</code>.
     */
    @Override
    public UsuariosRecord setUsusenha(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.usuarios.ususenha</code>.
     */
    @Override
    public String getUsusenha() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.usuarios.usuativo</code>.
     */
    @Override
    public UsuariosRecord setUsuativo(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>public.usuarios.usuativo</code>.
     */
    @Override
    public String getUsuativo() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.usuarios.fkfunmatricula</code>.
     */
    @Override
    public UsuariosRecord setFkfunmatricula(String value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>public.usuarios.fkfunmatricula</code>.
     */
    @Override
    public String getFkfunmatricula() {
        return (String) get(4);
    }

    /**
     * Setter for <code>public.usuarios.fkgrpid</code>.
     */
    @Override
    public UsuariosRecord setFkgrpid(Integer value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>public.usuarios.fkgrpid</code>.
     */
    @Override
    public Integer getFkgrpid() {
        return (Integer) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, String, Integer> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, String, Integer> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Usuarios.USUARIOS.USUID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Usuarios.USUARIOS.USUNOME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Usuarios.USUARIOS.USUSENHA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Usuarios.USUARIOS.USUATIVO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return Usuarios.USUARIOS.FKFUNMATRICULA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return Usuarios.USUARIOS.FKGRPID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getUsuid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getUsunome();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getUsusenha();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getUsuativo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getFkfunmatricula();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component6() {
        return getFkgrpid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getUsuid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getUsunome();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getUsusenha();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getUsuativo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getFkfunmatricula();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getFkgrpid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord value1(Integer value) {
        setUsuid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord value2(String value) {
        setUsunome(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord value3(String value) {
        setUsusenha(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord value4(String value) {
        setUsuativo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord value5(String value) {
        setFkfunmatricula(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord value6(Integer value) {
        setFkgrpid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UsuariosRecord values(Integer value1, String value2, String value3, String value4, String value5, Integer value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(IUsuarios from) {
        setUsuid(from.getUsuid());
        setUsunome(from.getUsunome());
        setUsusenha(from.getUsusenha());
        setUsuativo(from.getUsuativo());
        setFkfunmatricula(from.getFkfunmatricula());
        setFkgrpid(from.getFkgrpid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends IUsuarios> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UsuariosRecord
     */
    public UsuariosRecord() {
        super(Usuarios.USUARIOS);
    }

    /**
     * Create a detached, initialised UsuariosRecord
     */
    public UsuariosRecord(Integer usuid, String usunome, String ususenha, String usuativo, String fkfunmatricula, Integer fkgrpid) {
        super(Usuarios.USUARIOS);

        set(0, usuid);
        set(1, usunome);
        set(2, ususenha);
        set(3, usuativo);
        set(4, fkfunmatricula);
        set(5, fkgrpid);
    }

    public UsuariosRecord(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
