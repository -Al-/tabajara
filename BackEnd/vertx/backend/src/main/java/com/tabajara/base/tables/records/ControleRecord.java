/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.records;


import com.tabajara.base.tables.Controle;
import com.tabajara.base.tables.interfaces.IControle;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.sql.Date;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ControleRecord extends UpdatableRecordImpl<ControleRecord> implements VertxPojo, Record2<Integer, Date>, IControle {

    private static final long serialVersionUID = -508469889;

    /**
     * Setter for <code>public.controle.conid</code>.
     */
    @Override
    public ControleRecord setConid(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.controle.conid</code>.
     */
    @Override
    public Integer getConid() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.controle.concriacao</code>.
     */
    @Override
    public ControleRecord setConcriacao(Date value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.controle.concriacao</code>.
     */
    @Override
    public Date getConcriacao() {
        return (Date) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Integer, Date> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Integer, Date> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Controle.CONTROLE.CONID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field2() {
        return Controle.CONTROLE.CONCRIACAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getConid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date component2() {
        return getConcriacao();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getConid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value2() {
        return getConcriacao();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControleRecord value1(Integer value) {
        setConid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControleRecord value2(Date value) {
        setConcriacao(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControleRecord values(Integer value1, Date value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(IControle from) {
        setConid(from.getConid());
        setConcriacao(from.getConcriacao());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends IControle> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ControleRecord
     */
    public ControleRecord() {
        super(Controle.CONTROLE);
    }

    /**
     * Create a detached, initialised ControleRecord
     */
    public ControleRecord(Integer conid, Date concriacao) {
        super(Controle.CONTROLE);

        set(0, conid);
        set(1, concriacao);
    }

    public ControleRecord(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
