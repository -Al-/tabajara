/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.interfaces;


import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.io.Serializable;
import java.sql.Date;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public interface IControle extends VertxPojo, Serializable {

    /**
     * Setter for <code>public.controle.conid</code>.
     */
    public IControle setConid(Integer value);

    /**
     * Getter for <code>public.controle.conid</code>.
     */
    public Integer getConid();

    /**
     * Setter for <code>public.controle.concriacao</code>.
     */
    public IControle setConcriacao(Date value);

    /**
     * Getter for <code>public.controle.concriacao</code>.
     */
    public Date getConcriacao();

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * Load data from another generated Record/POJO implementing the common interface IControle
     */
    public void from(com.tabajara.base.tables.interfaces.IControle from);

    /**
     * Copy data into another generated Record/POJO implementing the common interface IControle
     */
    public <E extends com.tabajara.base.tables.interfaces.IControle> E into(E into);

    @Override
    public default IControle fromJson(io.vertx.core.json.JsonObject json) {
        setConid(json.getInteger("conid"));
        // Omitting unrecognized type java.sql.Date for column concriacao!
        return this;
    }


    @Override
    public default io.vertx.core.json.JsonObject toJson() {
        io.vertx.core.json.JsonObject json = new io.vertx.core.json.JsonObject();
        json.put("conid",getConid());
        // Omitting unrecognized type java.sql.Date for column concriacao!
        return json;
    }

}
