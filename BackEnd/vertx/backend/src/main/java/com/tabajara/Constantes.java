package com.tabajara;

public class Constantes {
	Constantes() {
	}

	// arduino
	public static final String ARD_GET = "/arduino/:id";
	public static final String ARD_LIST_ALL = "/arduino";
	public static final String ARD_CREATE = "/arduino";
	public static final String ARD_UPDATE = "/arduino/:id";
	public static final String ARD_DELETE = "/arduino/:id";
	public static final String ARD_DELETE_ALL = "/arduino";
	// arduino
	public static final String cri_GET = "/criacao/:id";
	public static final String cri_LIST_ALL = "/criacao";
	public static final String cri_CREATE = "/criacao";
	public static final String cri = "/criacao/:id";
	public static final String cri_DELETE = "/criacao/:id";
	public static final String cri_DELETE_ALL = "/criacao";
	// empresa
	public static final String EMP_GET = "/empresa/:id";
	public static final String EMP_LIST_ALL = "/empresa";
	public static final String EMP_CREATE = "/empresa";
	public static final String EMP_UPDATE = "/empresa/:id";
	public static final String EMP_DELETE = "/empresa/:id";
	public static final String EMP_DELETE_ALL = "/empresa";
	// funcionario
	public static final String FUN_GET = "/funcionario/:id";
	public static final String FUN_LIST_ALL = "/funcionario";
	public static final String FUN_CREATE = "/funcionario";
	public static final String FUN_UPDATE = "/funcionario/:id";
	public static final String FUN_DELETE = "/funcionario/:id";
	public static final String FUN_DELETE_ALL = "/funcionario";
	// telefones
	public static final String TEL_GET = "/telefones/:id";
	public static final String TEL_LIST_ALL = "/telefones";
	public static final String TEL_CREATE = "/telefones";
	public static final String TEL_UPDATE = "/telefones/:id";
	public static final String TEL_DELETE = "/telefones/:id";
	public static final String TEL_DELETE_ALL = "/telefones";
	// grpacesso
	public static final String GRP_GET = "/grpacesso/:id";
	public static final String GRP_LIST_ALL = "/grpacesso";
	public static final String GRP_CREATE = "/grpacesso";
	public static final String GRP_UPDATE = "/grpacesso/:id";
	public static final String GRP_DELETE = "/grpacesso/:id";
	public static final String GRP_DELETE_ALL = "/grpacesso";
	// usuarios
	public static final String USU_GET = "/usuarios/:id";
	public static final String USU_LIST_ALL = "/usuarios";
	public static final String USU_CREATE = "/usuarios";
	public static final String USU_UPDATE = "/usuarios/:id";
	public static final String USU_DELETE = "/usuarios/:id";
	public static final String USU_DELETE_ALL = "/usuarios";
	// tiposensor
	public static final String TIPSEN_GET = "/tiposensor/:id";
	public static final String TIPSEN_LIST_ALL = "/tiposensor";
	public static final String TIPSEN_CREATE = "/tiposensor";
	public static final String TIPSEN_UPDATE = "/tiposensor/:id";
	public static final String TIPSEN_DELETE = "/tiposensor/:id";
	public static final String TIPSEN_DELETE_ALL = "/tiposensor";
	// tipoatuador
	public static final String TIPATU_GET = "/tipoatuador/:id";
	public static final String TIPATU_LIST_ALL = "/tipoatuador";
	public static final String TIPATU_CREATE = "/tipoatuador";
	public static final String TIPATU_UPDATE = "/tipoatuador/:id";
	public static final String TIPATU_DELETE = "/tipoatuador/:id";
	public static final String TIPATU_DELETE_ALL = "/tipoatuador";
	// tiporepeticao
	public static final String TIPREP_GET = "/tiporepeticao/:id";
	public static final String TIPREP_LIST_ALL = "/tiporepeticao";
	public static final String TIPREP_CREATE = "/tiporepeticao";
	public static final String TIPREP_UPDATE = "/tiporepeticao/:id";
	public static final String TIPREP_DELETE = "/tiporepeticao/:id";
	public static final String TIPREP_DELETE_ALL = "/tiporepeticao";
	// sensor
	public static final String SEN_GET = "/sensor/:id";
	public static final String SEN_LIST_ALL = "/sensor";
	public static final String SEN_CREATE = "/sensor";
	public static final String SEN_UPDATE = "/sensor/:id";
	public static final String SEN_DELETE = "/sensor/:id";
	public static final String SEN_DELETE_ALL = "/sensor";
	// atuador
	public static final String ATUA_GET = "/atuador/:id";
	public static final String ATUA_LIST_ALL = "/atuador";
	public static final String ATUA_CREATE = "/atuador";
	public static final String ATUA_UPDATE = "/atuador/:id";
	public static final String ATUA_DELETE = "/atuador/:id";
	public static final String ATUA_DELETE_ALL = "/atuador";
	// horaRepeticao
	public static final String HREP_GET = "/horaRepeticao/:id";
	public static final String HREP_LIST_ALL = "/horaRepeticao";
	public static final String HREP_CREATE = "/horaRepeticao";
	public static final String HREP_UPDATE = "/horaRepeticao/:id";
	public static final String HREP_DELETE = "/horaRepeticao/:id";
	public static final String HREP_DELETE_ALL = "/horaRepeticao";
	// tanque
	public static final String TAN_GET = "/tanque/:id";
	public static final String TAN_LIST_ALL = "/tanque";
	public static final String TAN_CREATE = "/tanque";
	public static final String TAN_UPDATE = "/tanque/:id";
	public static final String TAN_DELETE = "/tanque/:id";
	public static final String TAN_DELETE_ALL = "/tanque";
	// controle
	public static final String CONT_GET = "/controle/:id";
	public static final String CONT_LIST_ALL = "/controle";
	public static final String CONT_CREATE = "/controle";
	public static final String CONT_UPDATE = "/controle/:id";
	public static final String CONT_DELETE = "/controle/:id";
	public static final String CONT_DELETE_ALL = "/controle";
}
