package com.tabajara;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;

public class MainVerticle extends AbstractVerticle {
	private HttpServer server;

	@Override
	public void start(Future<Void> future) {

		server = createServer();
		server.requestHandler(new MainVerticleAPI(vertx, config()));
		server.listen(res -> {
			if (res.failed()) {
				future.fail(res.cause());
			} else {
				future.complete();
			}
		});
	}

	@Override
	public void stop(Future<Void> future) {
		if (server == null) {
			future.complete();
			return;
		}
		server.close(future.completer());
	}

	private HttpServer createServer() {
		HttpServerOptions options = new HttpServerOptions();
		options.setHost(config().getString("http.address", "127.0.0.1"));
		options.setPort(config().getInteger("http.port", 8088));
		return vertx.createHttpServer(options);
	}

}
