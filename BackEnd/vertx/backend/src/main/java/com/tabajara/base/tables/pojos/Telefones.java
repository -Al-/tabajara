/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.pojos;


import com.tabajara.base.tables.interfaces.ITelefones;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Telefones implements VertxPojo, ITelefones {

    private static final long serialVersionUID = -494062847;

    private Integer telid;
    private String  telnumero;
    private String  fkfunmatricula;

    public Telefones() {}

    public Telefones(Telefones value) {
        this.telid = value.telid;
        this.telnumero = value.telnumero;
        this.fkfunmatricula = value.fkfunmatricula;
    }

    public Telefones(
        Integer telid,
        String  telnumero,
        String  fkfunmatricula
    ) {
        this.telid = telid;
        this.telnumero = telnumero;
        this.fkfunmatricula = fkfunmatricula;
    }

    @Override
    public Integer getTelid() {
        return this.telid;
    }

    @Override
    public Telefones setTelid(Integer telid) {
        this.telid = telid;
        return this;
    }

    @Override
    public String getTelnumero() {
        return this.telnumero;
    }

    @Override
    public Telefones setTelnumero(String telnumero) {
        this.telnumero = telnumero;
        return this;
    }

    @Override
    public String getFkfunmatricula() {
        return this.fkfunmatricula;
    }

    @Override
    public Telefones setFkfunmatricula(String fkfunmatricula) {
        this.fkfunmatricula = fkfunmatricula;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Telefones (");

        sb.append(telid);
        sb.append(", ").append(telnumero);
        sb.append(", ").append(fkfunmatricula);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(ITelefones from) {
        setTelid(from.getTelid());
        setTelnumero(from.getTelnumero());
        setFkfunmatricula(from.getFkfunmatricula());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends ITelefones> E into(E into) {
        into.from(this);
        return into;
    }

    public Telefones(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
