/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.interfaces;


import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.io.Serializable;
import java.sql.Date;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public interface IHisatuacao extends VertxPojo, Serializable {

    /**
     * Setter for <code>public.hisatuacao.histatuaid</code>.
     */
    public IHisatuacao setHistatuaid(Integer value);

    /**
     * Getter for <code>public.hisatuacao.histatuaid</code>.
     */
    public Integer getHistatuaid();

    /**
     * Setter for <code>public.hisatuacao.fkardid</code>.
     */
    public IHisatuacao setFkardid(Integer value);

    /**
     * Getter for <code>public.hisatuacao.fkardid</code>.
     */
    public Integer getFkardid();

    /**
     * Setter for <code>public.hisatuacao.fktanid</code>.
     */
    public IHisatuacao setFktanid(Integer value);

    /**
     * Getter for <code>public.hisatuacao.fktanid</code>.
     */
    public Integer getFktanid();

    /**
     * Setter for <code>public.hisatuacao.fkatuid</code>.
     */
    public IHisatuacao setFkatuid(Integer value);

    /**
     * Getter for <code>public.hisatuacao.fkatuid</code>.
     */
    public Integer getFkatuid();

    /**
     * Setter for <code>public.hisatuacao.fkempid</code>.
     */
    public IHisatuacao setFkempid(Integer value);

    /**
     * Getter for <code>public.hisatuacao.fkempid</code>.
     */
    public Integer getFkempid();

    /**
     * Setter for <code>public.hisatuacao.hora</code>.
     */
    public IHisatuacao setHora(String value);

    /**
     * Getter for <code>public.hisatuacao.hora</code>.
     */
    public String getHora();

    /**
     * Setter for <code>public.hisatuacao.dia</code>.
     */
    public IHisatuacao setDia(Date value);

    /**
     * Getter for <code>public.hisatuacao.dia</code>.
     */
    public Date getDia();

    /**
     * Setter for <code>public.hisatuacao.medida</code>.
     */
    public IHisatuacao setMedida(String value);

    /**
     * Getter for <code>public.hisatuacao.medida</code>.
     */
    public String getMedida();

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * Load data from another generated Record/POJO implementing the common interface IHisatuacao
     */
    public void from(com.tabajara.base.tables.interfaces.IHisatuacao from);

    /**
     * Copy data into another generated Record/POJO implementing the common interface IHisatuacao
     */
    public <E extends com.tabajara.base.tables.interfaces.IHisatuacao> E into(E into);

    @Override
    public default IHisatuacao fromJson(io.vertx.core.json.JsonObject json) {
        setHistatuaid(json.getInteger("histatuaid"));
        setFkardid(json.getInteger("fkardid"));
        setFktanid(json.getInteger("fktanid"));
        setFkatuid(json.getInteger("fkatuid"));
        setFkempid(json.getInteger("fkempid"));
        setHora(json.getString("hora"));
        // Omitting unrecognized type java.sql.Date for column dia!
        setMedida(json.getString("medida"));
        return this;
    }


    @Override
    public default io.vertx.core.json.JsonObject toJson() {
        io.vertx.core.json.JsonObject json = new io.vertx.core.json.JsonObject();
        json.put("histatuaid",getHistatuaid());
        json.put("fkardid",getFkardid());
        json.put("fktanid",getFktanid());
        json.put("fkatuid",getFkatuid());
        json.put("fkempid",getFkempid());
        json.put("hora",getHora());
        // Omitting unrecognized type java.sql.Date for column dia!
        json.put("medida",getMedida());
        return json;
    }

}
