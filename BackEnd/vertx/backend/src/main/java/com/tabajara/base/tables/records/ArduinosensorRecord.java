/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.records;


import com.tabajara.base.tables.Arduinosensor;
import com.tabajara.base.tables.interfaces.IArduinosensor;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ArduinosensorRecord extends UpdatableRecordImpl<ArduinosensorRecord> implements VertxPojo, Record3<Integer, Integer, Integer>, IArduinosensor {

    private static final long serialVersionUID = 1848297993;

    /**
     * Setter for <code>public.arduinosensor.ardsenid</code>.
     */
    @Override
    public ArduinosensorRecord setArdsenid(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.arduinosensor.ardsenid</code>.
     */
    @Override
    public Integer getArdsenid() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.arduinosensor.fkardid</code>.
     */
    @Override
    public ArduinosensorRecord setFkardid(Integer value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.arduinosensor.fkardid</code>.
     */
    @Override
    public Integer getFkardid() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>public.arduinosensor.fksenid</code>.
     */
    @Override
    public ArduinosensorRecord setFksenid(Integer value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.arduinosensor.fksenid</code>.
     */
    @Override
    public Integer getFksenid() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, Integer, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, Integer, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Arduinosensor.ARDUINOSENSOR.ARDSENID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return Arduinosensor.ARDUINOSENSOR.FKARDID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return Arduinosensor.ARDUINOSENSOR.FKSENID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getArdsenid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component2() {
        return getFkardid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getFksenid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getArdsenid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getFkardid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getFksenid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinosensorRecord value1(Integer value) {
        setArdsenid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinosensorRecord value2(Integer value) {
        setFkardid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinosensorRecord value3(Integer value) {
        setFksenid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArduinosensorRecord values(Integer value1, Integer value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(IArduinosensor from) {
        setArdsenid(from.getArdsenid());
        setFkardid(from.getFkardid());
        setFksenid(from.getFksenid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends IArduinosensor> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ArduinosensorRecord
     */
    public ArduinosensorRecord() {
        super(Arduinosensor.ARDUINOSENSOR);
    }

    /**
     * Create a detached, initialised ArduinosensorRecord
     */
    public ArduinosensorRecord(Integer ardsenid, Integer fkardid, Integer fksenid) {
        super(Arduinosensor.ARDUINOSENSOR);

        set(0, ardsenid);
        set(1, fkardid);
        set(2, fksenid);
    }

    public ArduinosensorRecord(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
