/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.pojos;


import com.tabajara.base.tables.interfaces.IArduinocontrole;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Arduinocontrole implements VertxPojo, IArduinocontrole {

    private static final long serialVersionUID = -63676781;

    private Integer ardcontid;
    private Integer fkardid;
    private Integer fkconid;

    public Arduinocontrole() {}

    public Arduinocontrole(Arduinocontrole value) {
        this.ardcontid = value.ardcontid;
        this.fkardid = value.fkardid;
        this.fkconid = value.fkconid;
    }

    public Arduinocontrole(
        Integer ardcontid,
        Integer fkardid,
        Integer fkconid
    ) {
        this.ardcontid = ardcontid;
        this.fkardid = fkardid;
        this.fkconid = fkconid;
    }

    @Override
    public Integer getArdcontid() {
        return this.ardcontid;
    }

    @Override
    public Arduinocontrole setArdcontid(Integer ardcontid) {
        this.ardcontid = ardcontid;
        return this;
    }

    @Override
    public Integer getFkardid() {
        return this.fkardid;
    }

    @Override
    public Arduinocontrole setFkardid(Integer fkardid) {
        this.fkardid = fkardid;
        return this;
    }

    @Override
    public Integer getFkconid() {
        return this.fkconid;
    }

    @Override
    public Arduinocontrole setFkconid(Integer fkconid) {
        this.fkconid = fkconid;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Arduinocontrole (");

        sb.append(ardcontid);
        sb.append(", ").append(fkardid);
        sb.append(", ").append(fkconid);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(IArduinocontrole from) {
        setArdcontid(from.getArdcontid());
        setFkardid(from.getFkardid());
        setFkconid(from.getFkconid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends IArduinocontrole> E into(E into) {
        into.from(this);
        return into;
    }

    public Arduinocontrole(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
