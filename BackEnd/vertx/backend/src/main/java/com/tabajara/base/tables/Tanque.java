/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables;


import com.tabajara.base.Indexes;
import com.tabajara.base.Keys;
import com.tabajara.base.Public;
import com.tabajara.base.tables.records.TanqueRecord;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tanque extends TableImpl<TanqueRecord> {

    private static final long serialVersionUID = -1118884628;

    /**
     * The reference instance of <code>public.tanque</code>
     */
    public static final Tanque TANQUE = new Tanque();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TanqueRecord> getRecordType() {
        return TanqueRecord.class;
    }

    /**
     * The column <code>public.tanque.tanid</code>.
     */
    public final TableField<TanqueRecord, Integer> TANID = createField("tanid", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('tanque_tanid_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.tanque.tanidentificacao</code>.
     */
    public final TableField<TanqueRecord, String> TANIDENTIFICACAO = createField("tanidentificacao", org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.tanque.tandatainicio</code>.
     */
    public final TableField<TanqueRecord, Date> TANDATAINICIO = createField("tandatainicio", org.jooq.impl.SQLDataType.DATE.defaultValue(org.jooq.impl.DSL.field("CURRENT_DATE", org.jooq.impl.SQLDataType.DATE)), this, "");

    /**
     * The column <code>public.tanque.fkcriacao</code>.
     */
    public final TableField<TanqueRecord, Integer> FKCRIACAO = createField("fkcriacao", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>public.tanque</code> table reference
     */
    public Tanque() {
        this(DSL.name("tanque"), null);
    }

    /**
     * Create an aliased <code>public.tanque</code> table reference
     */
    public Tanque(String alias) {
        this(DSL.name(alias), TANQUE);
    }

    /**
     * Create an aliased <code>public.tanque</code> table reference
     */
    public Tanque(Name alias) {
        this(alias, TANQUE);
    }

    private Tanque(Name alias, Table<TanqueRecord> aliased) {
        this(alias, aliased, null);
    }

    private Tanque(Name alias, Table<TanqueRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.TANQUE_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<TanqueRecord, Integer> getIdentity() {
        return Keys.IDENTITY_TANQUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TanqueRecord> getPrimaryKey() {
        return Keys.TANQUE_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TanqueRecord>> getKeys() {
        return Arrays.<UniqueKey<TanqueRecord>>asList(Keys.TANQUE_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<TanqueRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<TanqueRecord, ?>>asList(Keys.TANQUE__TANQUE_FKCRIACAO_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tanque as(String alias) {
        return new Tanque(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tanque as(Name alias) {
        return new Tanque(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Tanque rename(String name) {
        return new Tanque(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Tanque rename(Name name) {
        return new Tanque(name, null);
    }
}
