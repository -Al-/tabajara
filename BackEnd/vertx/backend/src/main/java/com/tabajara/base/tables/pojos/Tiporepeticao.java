/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.pojos;


import com.tabajara.base.tables.interfaces.ITiporepeticao;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tiporepeticao implements VertxPojo, ITiporepeticao {

    private static final long serialVersionUID = 1778682857;

    private Integer tiprepid;
    private String  tiprepnome;
    private String  tiprepdescricao;

    public Tiporepeticao() {}

    public Tiporepeticao(Tiporepeticao value) {
        this.tiprepid = value.tiprepid;
        this.tiprepnome = value.tiprepnome;
        this.tiprepdescricao = value.tiprepdescricao;
    }

    public Tiporepeticao(
        Integer tiprepid,
        String  tiprepnome,
        String  tiprepdescricao
    ) {
        this.tiprepid = tiprepid;
        this.tiprepnome = tiprepnome;
        this.tiprepdescricao = tiprepdescricao;
    }

    @Override
    public Integer getTiprepid() {
        return this.tiprepid;
    }

    @Override
    public Tiporepeticao setTiprepid(Integer tiprepid) {
        this.tiprepid = tiprepid;
        return this;
    }

    @Override
    public String getTiprepnome() {
        return this.tiprepnome;
    }

    @Override
    public Tiporepeticao setTiprepnome(String tiprepnome) {
        this.tiprepnome = tiprepnome;
        return this;
    }

    @Override
    public String getTiprepdescricao() {
        return this.tiprepdescricao;
    }

    @Override
    public Tiporepeticao setTiprepdescricao(String tiprepdescricao) {
        this.tiprepdescricao = tiprepdescricao;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Tiporepeticao (");

        sb.append(tiprepid);
        sb.append(", ").append(tiprepnome);
        sb.append(", ").append(tiprepdescricao);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(ITiporepeticao from) {
        setTiprepid(from.getTiprepid());
        setTiprepnome(from.getTiprepnome());
        setTiprepdescricao(from.getTiprepdescricao());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends ITiporepeticao> E into(E into) {
        into.from(this);
        return into;
    }

    public Tiporepeticao(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
