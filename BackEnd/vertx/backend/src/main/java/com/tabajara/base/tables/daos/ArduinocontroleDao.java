/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.daos;


import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;

import com.tabajara.base.tables.Arduinocontrole;
import com.tabajara.base.tables.records.ArduinocontroleRecord;

import io.github.jklingsporn.vertx.jooq.classic.jdbc.JDBCClassicQueryExecutor;
import io.github.jklingsporn.vertx.jooq.shared.internal.AbstractVertxDAO;
import io.vertx.core.Future;
/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ArduinocontroleDao extends AbstractVertxDAO<ArduinocontroleRecord, com.tabajara.base.tables.pojos.Arduinocontrole, Integer, Future<List<com.tabajara.base.tables.pojos.Arduinocontrole>>, Future<com.tabajara.base.tables.pojos.Arduinocontrole>, Future<Integer>, Future<Integer>> implements io.github.jklingsporn.vertx.jooq.classic.VertxDAO<ArduinocontroleRecord,com.tabajara.base.tables.pojos.Arduinocontrole,Integer> {

    /**
     * @param configuration The Configuration used for rendering and query execution.
     * @param vertx the vertx instance
     */
    public ArduinocontroleDao(Configuration configuration, io.vertx.core.Vertx vertx) {
        super(Arduinocontrole.ARDUINOCONTROLE, com.tabajara.base.tables.pojos.Arduinocontrole.class, new JDBCClassicQueryExecutor<ArduinocontroleRecord,com.tabajara.base.tables.pojos.Arduinocontrole,Integer>(com.tabajara.base.tables.pojos.Arduinocontrole.class,configuration,vertx), configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(com.tabajara.base.tables.pojos.Arduinocontrole object) {
        return object.getArdcontid();
    }

    /**
     * Find records that have <code>fkardid IN (values)</code> asynchronously
     */
    public Future<List<com.tabajara.base.tables.pojos.Arduinocontrole>> findManyByFkardid(List<Integer> values) {
        return findManyByCondition(Arduinocontrole.ARDUINOCONTROLE.FKARDID.in(values));
    }

    /**
     * Find records that have <code>fkconid IN (values)</code> asynchronously
     */
    public Future<List<com.tabajara.base.tables.pojos.Arduinocontrole>> findManyByFkconid(List<Integer> values) {
        return findManyByCondition(Arduinocontrole.ARDUINOCONTROLE.FKCONID.in(values));
    }
}
