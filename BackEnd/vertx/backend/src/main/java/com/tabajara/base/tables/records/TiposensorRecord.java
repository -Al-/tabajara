/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.records;


import com.tabajara.base.tables.Tiposensor;
import com.tabajara.base.tables.interfaces.ITiposensor;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TiposensorRecord extends UpdatableRecordImpl<TiposensorRecord> implements VertxPojo, Record3<Integer, String, String>, ITiposensor {

    private static final long serialVersionUID = 1877394643;

    /**
     * Setter for <code>public.tiposensor.tipsenid</code>.
     */
    @Override
    public TiposensorRecord setTipsenid(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.tiposensor.tipsenid</code>.
     */
    @Override
    public Integer getTipsenid() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.tiposensor.tipsennome</code>.
     */
    @Override
    public TiposensorRecord setTipsennome(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.tiposensor.tipsennome</code>.
     */
    @Override
    public String getTipsennome() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.tiposensor.tipsendescricao</code>.
     */
    @Override
    public TiposensorRecord setTipsendescricao(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.tiposensor.tipsendescricao</code>.
     */
    @Override
    public String getTipsendescricao() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, String, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Tiposensor.TIPOSENSOR.TIPSENID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Tiposensor.TIPOSENSOR.TIPSENNOME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Tiposensor.TIPOSENSOR.TIPSENDESCRICAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getTipsenid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getTipsennome();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getTipsendescricao();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getTipsenid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getTipsennome();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getTipsendescricao();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiposensorRecord value1(Integer value) {
        setTipsenid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiposensorRecord value2(String value) {
        setTipsennome(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiposensorRecord value3(String value) {
        setTipsendescricao(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiposensorRecord values(Integer value1, String value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(ITiposensor from) {
        setTipsenid(from.getTipsenid());
        setTipsennome(from.getTipsennome());
        setTipsendescricao(from.getTipsendescricao());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends ITiposensor> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TiposensorRecord
     */
    public TiposensorRecord() {
        super(Tiposensor.TIPOSENSOR);
    }

    /**
     * Create a detached, initialised TiposensorRecord
     */
    public TiposensorRecord(Integer tipsenid, String tipsennome, String tipsendescricao) {
        super(Tiposensor.TIPOSENSOR);

        set(0, tipsenid);
        set(1, tipsennome);
        set(2, tipsendescricao);
    }

    public TiposensorRecord(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
