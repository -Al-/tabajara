/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.records;


import com.tabajara.base.tables.Tiporepeticao;
import com.tabajara.base.tables.interfaces.ITiporepeticao;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TiporepeticaoRecord extends UpdatableRecordImpl<TiporepeticaoRecord> implements VertxPojo, Record3<Integer, String, String>, ITiporepeticao {

    private static final long serialVersionUID = 935831233;

    /**
     * Setter for <code>public.tiporepeticao.tiprepid</code>.
     */
    @Override
    public TiporepeticaoRecord setTiprepid(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.tiporepeticao.tiprepid</code>.
     */
    @Override
    public Integer getTiprepid() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.tiporepeticao.tiprepnome</code>.
     */
    @Override
    public TiporepeticaoRecord setTiprepnome(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.tiporepeticao.tiprepnome</code>.
     */
    @Override
    public String getTiprepnome() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.tiporepeticao.tiprepdescricao</code>.
     */
    @Override
    public TiporepeticaoRecord setTiprepdescricao(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.tiporepeticao.tiprepdescricao</code>.
     */
    @Override
    public String getTiprepdescricao() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, String, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Tiporepeticao.TIPOREPETICAO.TIPREPID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Tiporepeticao.TIPOREPETICAO.TIPREPNOME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Tiporepeticao.TIPOREPETICAO.TIPREPDESCRICAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getTiprepid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getTiprepnome();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getTiprepdescricao();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getTiprepid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getTiprepnome();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getTiprepdescricao();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiporepeticaoRecord value1(Integer value) {
        setTiprepid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiporepeticaoRecord value2(String value) {
        setTiprepnome(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiporepeticaoRecord value3(String value) {
        setTiprepdescricao(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TiporepeticaoRecord values(Integer value1, String value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(ITiporepeticao from) {
        setTiprepid(from.getTiprepid());
        setTiprepnome(from.getTiprepnome());
        setTiprepdescricao(from.getTiprepdescricao());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends ITiporepeticao> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TiporepeticaoRecord
     */
    public TiporepeticaoRecord() {
        super(Tiporepeticao.TIPOREPETICAO);
    }

    /**
     * Create a detached, initialised TiporepeticaoRecord
     */
    public TiporepeticaoRecord(Integer tiprepid, String tiprepnome, String tiprepdescricao) {
        super(Tiporepeticao.TIPOREPETICAO);

        set(0, tiprepid);
        set(1, tiprepnome);
        set(2, tiprepdescricao);
    }

    public TiporepeticaoRecord(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
