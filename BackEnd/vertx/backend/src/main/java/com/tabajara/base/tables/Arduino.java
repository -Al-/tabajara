/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables;


import com.tabajara.base.Indexes;
import com.tabajara.base.Keys;
import com.tabajara.base.Public;
import com.tabajara.base.tables.records.ArduinoRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Arduino extends TableImpl<ArduinoRecord> {

    private static final long serialVersionUID = -551388202;

    /**
     * The reference instance of <code>public.arduino</code>
     */
    public static final Arduino ARDUINO = new Arduino();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ArduinoRecord> getRecordType() {
        return ArduinoRecord.class;
    }

    /**
     * The column <code>public.arduino.ardid</code>.
     */
    public final TableField<ArduinoRecord, Integer> ARDID = createField("ardid", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('arduino_ardid_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>public.arduino.ardlocal</code>.
     */
    public final TableField<ArduinoRecord, String> ARDLOCAL = createField("ardlocal", org.jooq.impl.SQLDataType.VARCHAR(50), this, "");

    /**
     * The column <code>public.arduino.ardstatus</code>.
     */
    public final TableField<ArduinoRecord, String> ARDSTATUS = createField("ardstatus", org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * The column <code>public.arduino.ardvirtual</code>.
     */
    public final TableField<ArduinoRecord, String> ARDVIRTUAL = createField("ardvirtual", org.jooq.impl.SQLDataType.VARCHAR(20), this, "");

    /**
     * The column <code>public.arduino.ardativo</code>.
     */
    public final TableField<ArduinoRecord, String> ARDATIVO = createField("ardativo", org.jooq.impl.SQLDataType.VARCHAR(20), this, "");

    /**
     * The column <code>public.arduino.fktanid</code>.
     */
    public final TableField<ArduinoRecord, Integer> FKTANID = createField("fktanid", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>public.arduino</code> table reference
     */
    public Arduino() {
        this(DSL.name("arduino"), null);
    }

    /**
     * Create an aliased <code>public.arduino</code> table reference
     */
    public Arduino(String alias) {
        this(DSL.name(alias), ARDUINO);
    }

    /**
     * Create an aliased <code>public.arduino</code> table reference
     */
    public Arduino(Name alias) {
        this(alias, ARDUINO);
    }

    private Arduino(Name alias, Table<ArduinoRecord> aliased) {
        this(alias, aliased, null);
    }

    private Arduino(Name alias, Table<ArduinoRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.ARDUINO_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<ArduinoRecord, Integer> getIdentity() {
        return Keys.IDENTITY_ARDUINO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<ArduinoRecord> getPrimaryKey() {
        return Keys.ARDUINO_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<ArduinoRecord>> getKeys() {
        return Arrays.<UniqueKey<ArduinoRecord>>asList(Keys.ARDUINO_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<ArduinoRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<ArduinoRecord, ?>>asList(Keys.ARDUINO__ARDUINO_FKTANID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Arduino as(String alias) {
        return new Arduino(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Arduino as(Name alias) {
        return new Arduino(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Arduino rename(String name) {
        return new Arduino(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Arduino rename(Name name) {
        return new Arduino(name, null);
    }
}
