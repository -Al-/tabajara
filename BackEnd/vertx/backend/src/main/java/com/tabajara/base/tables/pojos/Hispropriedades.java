/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.pojos;


import com.tabajara.base.tables.interfaces.IHispropriedades;

import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.sql.Date;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Hispropriedades implements VertxPojo, IHispropriedades {

    private static final long serialVersionUID = -977313236;

    private Integer hispoptid;
    private Integer fkardid;
    private Integer fktanid;
    private Integer fksenid;
    private Integer fkempid;
    private String  hora;
    private Date    dia;
    private String  medida;

    public Hispropriedades() {}

    public Hispropriedades(Hispropriedades value) {
        this.hispoptid = value.hispoptid;
        this.fkardid = value.fkardid;
        this.fktanid = value.fktanid;
        this.fksenid = value.fksenid;
        this.fkempid = value.fkempid;
        this.hora = value.hora;
        this.dia = value.dia;
        this.medida = value.medida;
    }

    public Hispropriedades(
        Integer hispoptid,
        Integer fkardid,
        Integer fktanid,
        Integer fksenid,
        Integer fkempid,
        String  hora,
        Date    dia,
        String  medida
    ) {
        this.hispoptid = hispoptid;
        this.fkardid = fkardid;
        this.fktanid = fktanid;
        this.fksenid = fksenid;
        this.fkempid = fkempid;
        this.hora = hora;
        this.dia = dia;
        this.medida = medida;
    }

    @Override
    public Integer getHispoptid() {
        return this.hispoptid;
    }

    @Override
    public Hispropriedades setHispoptid(Integer hispoptid) {
        this.hispoptid = hispoptid;
        return this;
    }

    @Override
    public Integer getFkardid() {
        return this.fkardid;
    }

    @Override
    public Hispropriedades setFkardid(Integer fkardid) {
        this.fkardid = fkardid;
        return this;
    }

    @Override
    public Integer getFktanid() {
        return this.fktanid;
    }

    @Override
    public Hispropriedades setFktanid(Integer fktanid) {
        this.fktanid = fktanid;
        return this;
    }

    @Override
    public Integer getFksenid() {
        return this.fksenid;
    }

    @Override
    public Hispropriedades setFksenid(Integer fksenid) {
        this.fksenid = fksenid;
        return this;
    }

    @Override
    public Integer getFkempid() {
        return this.fkempid;
    }

    @Override
    public Hispropriedades setFkempid(Integer fkempid) {
        this.fkempid = fkempid;
        return this;
    }

    @Override
    public String getHora() {
        return this.hora;
    }

    @Override
    public Hispropriedades setHora(String hora) {
        this.hora = hora;
        return this;
    }

    @Override
    public Date getDia() {
        return this.dia;
    }

    @Override
    public Hispropriedades setDia(Date dia) {
        this.dia = dia;
        return this;
    }

    @Override
    public String getMedida() {
        return this.medida;
    }

    @Override
    public Hispropriedades setMedida(String medida) {
        this.medida = medida;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Hispropriedades (");

        sb.append(hispoptid);
        sb.append(", ").append(fkardid);
        sb.append(", ").append(fktanid);
        sb.append(", ").append(fksenid);
        sb.append(", ").append(fkempid);
        sb.append(", ").append(hora);
        sb.append(", ").append(dia);
        sb.append(", ").append(medida);

        sb.append(")");
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void from(IHispropriedades from) {
        setHispoptid(from.getHispoptid());
        setFkardid(from.getFkardid());
        setFktanid(from.getFktanid());
        setFksenid(from.getFksenid());
        setFkempid(from.getFkempid());
        setHora(from.getHora());
        setDia(from.getDia());
        setMedida(from.getMedida());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <E extends IHispropriedades> E into(E into) {
        into.from(this);
        return into;
    }

    public Hispropriedades(io.vertx.core.json.JsonObject json) {
        this();
        fromJson(json);
    }
}
