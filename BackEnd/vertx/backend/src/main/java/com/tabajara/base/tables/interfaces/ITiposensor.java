/*
 * This file is generated by jOOQ.
*/
package com.tabajara.base.tables.interfaces;


import io.github.jklingsporn.vertx.jooq.shared.internal.VertxPojo;

import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public interface ITiposensor extends VertxPojo, Serializable {

    /**
     * Setter for <code>public.tiposensor.tipsenid</code>.
     */
    public ITiposensor setTipsenid(Integer value);

    /**
     * Getter for <code>public.tiposensor.tipsenid</code>.
     */
    public Integer getTipsenid();

    /**
     * Setter for <code>public.tiposensor.tipsennome</code>.
     */
    public ITiposensor setTipsennome(String value);

    /**
     * Getter for <code>public.tiposensor.tipsennome</code>.
     */
    public String getTipsennome();

    /**
     * Setter for <code>public.tiposensor.tipsendescricao</code>.
     */
    public ITiposensor setTipsendescricao(String value);

    /**
     * Getter for <code>public.tiposensor.tipsendescricao</code>.
     */
    public String getTipsendescricao();

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    /**
     * Load data from another generated Record/POJO implementing the common interface ITiposensor
     */
    public void from(com.tabajara.base.tables.interfaces.ITiposensor from);

    /**
     * Copy data into another generated Record/POJO implementing the common interface ITiposensor
     */
    public <E extends com.tabajara.base.tables.interfaces.ITiposensor> E into(E into);

    @Override
    public default ITiposensor fromJson(io.vertx.core.json.JsonObject json) {
        setTipsenid(json.getInteger("tipsenid"));
        setTipsennome(json.getString("tipsennome"));
        setTipsendescricao(json.getString("tipsendescricao"));
        return this;
    }


    @Override
    public default io.vertx.core.json.JsonObject toJson() {
        io.vertx.core.json.JsonObject json = new io.vertx.core.json.JsonObject();
        json.put("tipsenid",getTipsenid());
        json.put("tipsennome",getTipsennome());
        json.put("tipsendescricao",getTipsendescricao());
        return json;
    }

}
