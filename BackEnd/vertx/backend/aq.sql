PGDMP                          v            tabajara    10.4    10.4 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    33959    tabajara    DATABASE     �   CREATE DATABASE tabajara WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';
    DROP DATABASE tabajara;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    34095    arduino    TABLE     �   CREATE TABLE public.arduino (
    ardid integer NOT NULL,
    ardlocal character varying(50),
    ardstatus character varying(100),
    ardvirtual character varying(20),
    ardativo character varying(20),
    fktanid integer
);
    DROP TABLE public.arduino;
       public         postgres    false    3            �            1259    34093    arduino_ardid_seq    SEQUENCE     �   CREATE SEQUENCE public.arduino_ardid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.arduino_ardid_seq;
       public       postgres    false    3    222            �           0    0    arduino_ardid_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.arduino_ardid_seq OWNED BY public.arduino.ardid;
            public       postgres    false    221            �            1259    34188    arduinoatuador    TABLE     p   CREATE TABLE public.arduinoatuador (
    ardsenid integer NOT NULL,
    fkardid integer,
    fkatuid integer
);
 "   DROP TABLE public.arduinoatuador;
       public         postgres    false    3            �            1259    34186    arduinoatuador_ardsenid_seq    SEQUENCE     �   CREATE SEQUENCE public.arduinoatuador_ardsenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.arduinoatuador_ardsenid_seq;
       public       postgres    false    3    232            �           0    0    arduinoatuador_ardsenid_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.arduinoatuador_ardsenid_seq OWNED BY public.arduinoatuador.ardsenid;
            public       postgres    false    231            �            1259    34224    arduinocontrole    TABLE     r   CREATE TABLE public.arduinocontrole (
    ardcontid integer NOT NULL,
    fkardid integer,
    fkconid integer
);
 #   DROP TABLE public.arduinocontrole;
       public         postgres    false    3            �            1259    34222    arduinocontrole_ardcontid_seq    SEQUENCE     �   CREATE SEQUENCE public.arduinocontrole_ardcontid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.arduinocontrole_ardcontid_seq;
       public       postgres    false    236    3            �           0    0    arduinocontrole_ardcontid_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.arduinocontrole_ardcontid_seq OWNED BY public.arduinocontrole.ardcontid;
            public       postgres    false    235            �            1259    34170    arduinosensor    TABLE     o   CREATE TABLE public.arduinosensor (
    ardsenid integer NOT NULL,
    fkardid integer,
    fksenid integer
);
 !   DROP TABLE public.arduinosensor;
       public         postgres    false    3            �            1259    34168    arduinosensor_ardsenid_seq    SEQUENCE     �   CREATE SEQUENCE public.arduinosensor_ardsenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.arduinosensor_ardsenid_seq;
       public       postgres    false    3    230            �           0    0    arduinosensor_ardsenid_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.arduinosensor_ardsenid_seq OWNED BY public.arduinosensor.ardsenid;
            public       postgres    false    229            �            1259    34060    atuador    TABLE     
  CREATE TABLE public.atuador (
    atuid integer NOT NULL,
    atudescricao character varying(50),
    atuparametro character varying(50),
    atuatualizacao integer,
    atuativo character varying(20),
    atuvirtual character varying(20),
    fktipatuid integer
);
    DROP TABLE public.atuador;
       public         postgres    false    3            �            1259    34058    atuador_atuid_seq    SEQUENCE     �   CREATE SEQUENCE public.atuador_atuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.atuador_atuid_seq;
       public       postgres    false    3    216            �           0    0    atuador_atuid_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.atuador_atuid_seq OWNED BY public.atuador.atuid;
            public       postgres    false    215            �            1259    34108    controle    TABLE     g   CREATE TABLE public.controle (
    conid integer NOT NULL,
    concriacao date DEFAULT CURRENT_DATE
);
    DROP TABLE public.controle;
       public         postgres    false    3            �            1259    34106    controle_conid_seq    SEQUENCE     �   CREATE SEQUENCE public.controle_conid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.controle_conid_seq;
       public       postgres    false    3    224            �           0    0    controle_conid_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.controle_conid_seq OWNED BY public.controle.conid;
            public       postgres    false    223            �            1259    34242    controlegrpacesso    TABLE     s   CREATE TABLE public.controlegrpacesso (
    congrpid integer NOT NULL,
    fkconid integer,
    fkgrpid integer
);
 %   DROP TABLE public.controlegrpacesso;
       public         postgres    false    3            �            1259    34240    controlegrpacesso_congrpid_seq    SEQUENCE     �   CREATE SEQUENCE public.controlegrpacesso_congrpid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.controlegrpacesso_congrpid_seq;
       public       postgres    false    238    3            �           0    0    controlegrpacesso_congrpid_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.controlegrpacesso_congrpid_seq OWNED BY public.controlegrpacesso.congrpid;
            public       postgres    false    237            �            1259    33970    criacao    TABLE     �   CREATE TABLE public.criacao (
    criaid integer NOT NULL,
    criaespecie character varying(50),
    criadata date DEFAULT CURRENT_DATE
);
    DROP TABLE public.criacao;
       public         postgres    false    3            �            1259    33968    criacao_criaid_seq    SEQUENCE     �   CREATE SEQUENCE public.criacao_criaid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.criacao_criaid_seq;
       public       postgres    false    199    3            �           0    0    criacao_criaid_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.criacao_criaid_seq OWNED BY public.criacao.criaid;
            public       postgres    false    198            �            1259    33962    empresa    TABLE     �   CREATE TABLE public.empresa (
    empid integer NOT NULL,
    empnome character varying(50),
    empcnpj character varying(50),
    emplocal character varying(100)
);
    DROP TABLE public.empresa;
       public         postgres    false    3            �            1259    33960    empresa_empid_seq    SEQUENCE     �   CREATE SEQUENCE public.empresa_empid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.empresa_empid_seq;
       public       postgres    false    3    197            �           0    0    empresa_empid_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.empresa_empid_seq OWNED BY public.empresa.empid;
            public       postgres    false    196            �            1259    33977    funcionario    TABLE     �   CREATE TABLE public.funcionario (
    funmatricula character varying(30) NOT NULL,
    funnome character varying(50),
    funcpf character varying(14),
    funsexo character varying(14),
    funativo character varying(14)
);
    DROP TABLE public.funcionario;
       public         postgres    false    3            �            1259    33997 	   grpacesso    TABLE     |   CREATE TABLE public.grpacesso (
    grpid integer NOT NULL,
    grpdescricao character varying(50),
    grpativo boolean
);
    DROP TABLE public.grpacesso;
       public         postgres    false    3            �            1259    33995    grpacesso_grpid_seq    SEQUENCE     �   CREATE SEQUENCE public.grpacesso_grpid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.grpacesso_grpid_seq;
       public       postgres    false    3    204            �           0    0    grpacesso_grpid_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.grpacesso_grpid_seq OWNED BY public.grpacesso.grpid;
            public       postgres    false    203            �            1259    34117 	   historico    TABLE     "  CREATE TABLE public.historico (
    histid integer NOT NULL,
    fkardid integer,
    fktanid integer,
    fksenid integer,
    fkatuid integer,
    fkempid integer,
    hora character varying(50) DEFAULT CURRENT_TIME,
    dia date DEFAULT CURRENT_DATE,
    medida character varying(50)
);
    DROP TABLE public.historico;
       public         postgres    false    3            �            1259    34115    historico_histid_seq    SEQUENCE     �   CREATE SEQUENCE public.historico_histid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.historico_histid_seq;
       public       postgres    false    3    226            �           0    0    historico_histid_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.historico_histid_seq OWNED BY public.historico.histid;
            public       postgres    false    225            �            1259    34073    horarepeticao    TABLE     �   CREATE TABLE public.horarepeticao (
    horid integer NOT NULL,
    horhora character varying(5),
    hordiainicial date,
    horrepetiracada integer,
    hortiporepeticao character varying(50)
);
 !   DROP TABLE public.horarepeticao;
       public         postgres    false    3            �            1259    34071    horarepeticao_horid_seq    SEQUENCE     �   CREATE SEQUENCE public.horarepeticao_horid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.horarepeticao_horid_seq;
       public       postgres    false    218    3            �           0    0    horarepeticao_horid_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.horarepeticao_horid_seq OWNED BY public.horarepeticao.horid;
            public       postgres    false    217            �            1259    34047    sensor    TABLE     	  CREATE TABLE public.sensor (
    senid integer NOT NULL,
    sendescricao character varying(50),
    senparametro character varying(50),
    senatualizacao integer,
    senativo character varying(20),
    senvirtual character varying(20),
    fktipsenid integer
);
    DROP TABLE public.sensor;
       public         postgres    false    3            �            1259    34045    sensor_senid_seq    SEQUENCE     �   CREATE SEQUENCE public.sensor_senid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.sensor_senid_seq;
       public       postgres    false    214    3                        0    0    sensor_senid_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.sensor_senid_seq OWNED BY public.sensor.senid;
            public       postgres    false    213            �            1259    34206    sensorhorarepeticao    TABLE     u   CREATE TABLE public.sensorhorarepeticao (
    senhorid integer NOT NULL,
    fksenid integer,
    fkhorid integer
);
 '   DROP TABLE public.sensorhorarepeticao;
       public         postgres    false    3            �            1259    34204     sensorhorarepeticao_senhorid_seq    SEQUENCE     �   CREATE SEQUENCE public.sensorhorarepeticao_senhorid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.sensorhorarepeticao_senhorid_seq;
       public       postgres    false    234    3                       0    0     sensorhorarepeticao_senhorid_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.sensorhorarepeticao_senhorid_seq OWNED BY public.sensorhorarepeticao.senhorid;
            public       postgres    false    233            �            1259    34152    sensorrepeticao    TABLE     t   CREATE TABLE public.sensorrepeticao (
    senrepid integer NOT NULL,
    fktiprepid integer,
    fksenid integer
);
 #   DROP TABLE public.sensorrepeticao;
       public         postgres    false    3            �            1259    34150    sensorrepeticao_senrepid_seq    SEQUENCE     �   CREATE SEQUENCE public.sensorrepeticao_senrepid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.sensorrepeticao_senrepid_seq;
       public       postgres    false    228    3                       0    0    sensorrepeticao_senrepid_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.sensorrepeticao_senrepid_seq OWNED BY public.sensorrepeticao.senrepid;
            public       postgres    false    227            �            1259    34081    tanque    TABLE     �   CREATE TABLE public.tanque (
    tanid integer NOT NULL,
    tanidentificacao character varying(50),
    tandatainicio date DEFAULT CURRENT_DATE,
    fkcriacao integer
);
    DROP TABLE public.tanque;
       public         postgres    false    3            �            1259    34079    tanque_tanid_seq    SEQUENCE     �   CREATE SEQUENCE public.tanque_tanid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.tanque_tanid_seq;
       public       postgres    false    3    220                       0    0    tanque_tanid_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.tanque_tanid_seq OWNED BY public.tanque.tanid;
            public       postgres    false    219            �            1259    33984 	   telefones    TABLE     �   CREATE TABLE public.telefones (
    telid integer NOT NULL,
    telnumero character varying(11),
    fkfunmatricula character varying(30)
);
    DROP TABLE public.telefones;
       public         postgres    false    3            �            1259    33982    telefones_telid_seq    SEQUENCE     �   CREATE SEQUENCE public.telefones_telid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.telefones_telid_seq;
       public       postgres    false    3    202                       0    0    telefones_telid_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.telefones_telid_seq OWNED BY public.telefones.telid;
            public       postgres    false    201            �            1259    34031    tipoatuador    TABLE     �   CREATE TABLE public.tipoatuador (
    tipatuid integer NOT NULL,
    tipatunome character varying(50),
    tipatudescricao character varying(50)
);
    DROP TABLE public.tipoatuador;
       public         postgres    false    3            �            1259    34029    tipoatuador_tipatuid_seq    SEQUENCE     �   CREATE SEQUENCE public.tipoatuador_tipatuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.tipoatuador_tipatuid_seq;
       public       postgres    false    210    3                       0    0    tipoatuador_tipatuid_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.tipoatuador_tipatuid_seq OWNED BY public.tipoatuador.tipatuid;
            public       postgres    false    209            �            1259    34039    tiporepeticao    TABLE     �   CREATE TABLE public.tiporepeticao (
    tiprepid integer NOT NULL,
    tiprepnome character varying(50),
    tiprepdescricao character varying(50)
);
 !   DROP TABLE public.tiporepeticao;
       public         postgres    false    3            �            1259    34037    tiporepeticao_tiprepid_seq    SEQUENCE     �   CREATE SEQUENCE public.tiporepeticao_tiprepid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tiporepeticao_tiprepid_seq;
       public       postgres    false    212    3                       0    0    tiporepeticao_tiprepid_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tiporepeticao_tiprepid_seq OWNED BY public.tiporepeticao.tiprepid;
            public       postgres    false    211            �            1259    34023 
   tiposensor    TABLE     �   CREATE TABLE public.tiposensor (
    tipsenid integer NOT NULL,
    tipsennome character varying(50),
    tipsendescricao character varying(50)
);
    DROP TABLE public.tiposensor;
       public         postgres    false    3            �            1259    34021    tiposensor_tipsenid_seq    SEQUENCE     �   CREATE SEQUENCE public.tiposensor_tipsenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tiposensor_tipsenid_seq;
       public       postgres    false    208    3                       0    0    tiposensor_tipsenid_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tiposensor_tipsenid_seq OWNED BY public.tiposensor.tipsenid;
            public       postgres    false    207            �            1259    34005    usuarios    TABLE     �   CREATE TABLE public.usuarios (
    usuid integer NOT NULL,
    usunome character varying(20),
    ususenha character varying(20),
    usuativo character varying(20),
    fkfunmatricula character varying(30),
    fkgrpid integer
);
    DROP TABLE public.usuarios;
       public         postgres    false    3            �            1259    34003    usuarios_usuid_seq    SEQUENCE     �   CREATE SEQUENCE public.usuarios_usuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.usuarios_usuid_seq;
       public       postgres    false    206    3                       0    0    usuarios_usuid_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.usuarios_usuid_seq OWNED BY public.usuarios.usuid;
            public       postgres    false    205            �
           2604    34098    arduino ardid    DEFAULT     n   ALTER TABLE ONLY public.arduino ALTER COLUMN ardid SET DEFAULT nextval('public.arduino_ardid_seq'::regclass);
 <   ALTER TABLE public.arduino ALTER COLUMN ardid DROP DEFAULT;
       public       postgres    false    222    221    222                        2604    34191    arduinoatuador ardsenid    DEFAULT     �   ALTER TABLE ONLY public.arduinoatuador ALTER COLUMN ardsenid SET DEFAULT nextval('public.arduinoatuador_ardsenid_seq'::regclass);
 F   ALTER TABLE public.arduinoatuador ALTER COLUMN ardsenid DROP DEFAULT;
       public       postgres    false    231    232    232                       2604    34227    arduinocontrole ardcontid    DEFAULT     �   ALTER TABLE ONLY public.arduinocontrole ALTER COLUMN ardcontid SET DEFAULT nextval('public.arduinocontrole_ardcontid_seq'::regclass);
 H   ALTER TABLE public.arduinocontrole ALTER COLUMN ardcontid DROP DEFAULT;
       public       postgres    false    235    236    236            �
           2604    34173    arduinosensor ardsenid    DEFAULT     �   ALTER TABLE ONLY public.arduinosensor ALTER COLUMN ardsenid SET DEFAULT nextval('public.arduinosensor_ardsenid_seq'::regclass);
 E   ALTER TABLE public.arduinosensor ALTER COLUMN ardsenid DROP DEFAULT;
       public       postgres    false    230    229    230            �
           2604    34063    atuador atuid    DEFAULT     n   ALTER TABLE ONLY public.atuador ALTER COLUMN atuid SET DEFAULT nextval('public.atuador_atuid_seq'::regclass);
 <   ALTER TABLE public.atuador ALTER COLUMN atuid DROP DEFAULT;
       public       postgres    false    216    215    216            �
           2604    34111    controle conid    DEFAULT     p   ALTER TABLE ONLY public.controle ALTER COLUMN conid SET DEFAULT nextval('public.controle_conid_seq'::regclass);
 =   ALTER TABLE public.controle ALTER COLUMN conid DROP DEFAULT;
       public       postgres    false    223    224    224                       2604    34245    controlegrpacesso congrpid    DEFAULT     �   ALTER TABLE ONLY public.controlegrpacesso ALTER COLUMN congrpid SET DEFAULT nextval('public.controlegrpacesso_congrpid_seq'::regclass);
 I   ALTER TABLE public.controlegrpacesso ALTER COLUMN congrpid DROP DEFAULT;
       public       postgres    false    237    238    238            �
           2604    33973    criacao criaid    DEFAULT     p   ALTER TABLE ONLY public.criacao ALTER COLUMN criaid SET DEFAULT nextval('public.criacao_criaid_seq'::regclass);
 =   ALTER TABLE public.criacao ALTER COLUMN criaid DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    33965    empresa empid    DEFAULT     n   ALTER TABLE ONLY public.empresa ALTER COLUMN empid SET DEFAULT nextval('public.empresa_empid_seq'::regclass);
 <   ALTER TABLE public.empresa ALTER COLUMN empid DROP DEFAULT;
       public       postgres    false    197    196    197            �
           2604    34000    grpacesso grpid    DEFAULT     r   ALTER TABLE ONLY public.grpacesso ALTER COLUMN grpid SET DEFAULT nextval('public.grpacesso_grpid_seq'::regclass);
 >   ALTER TABLE public.grpacesso ALTER COLUMN grpid DROP DEFAULT;
       public       postgres    false    204    203    204            �
           2604    34120    historico histid    DEFAULT     t   ALTER TABLE ONLY public.historico ALTER COLUMN histid SET DEFAULT nextval('public.historico_histid_seq'::regclass);
 ?   ALTER TABLE public.historico ALTER COLUMN histid DROP DEFAULT;
       public       postgres    false    225    226    226            �
           2604    34076    horarepeticao horid    DEFAULT     z   ALTER TABLE ONLY public.horarepeticao ALTER COLUMN horid SET DEFAULT nextval('public.horarepeticao_horid_seq'::regclass);
 B   ALTER TABLE public.horarepeticao ALTER COLUMN horid DROP DEFAULT;
       public       postgres    false    218    217    218            �
           2604    34050    sensor senid    DEFAULT     l   ALTER TABLE ONLY public.sensor ALTER COLUMN senid SET DEFAULT nextval('public.sensor_senid_seq'::regclass);
 ;   ALTER TABLE public.sensor ALTER COLUMN senid DROP DEFAULT;
       public       postgres    false    213    214    214                       2604    34209    sensorhorarepeticao senhorid    DEFAULT     �   ALTER TABLE ONLY public.sensorhorarepeticao ALTER COLUMN senhorid SET DEFAULT nextval('public.sensorhorarepeticao_senhorid_seq'::regclass);
 K   ALTER TABLE public.sensorhorarepeticao ALTER COLUMN senhorid DROP DEFAULT;
       public       postgres    false    233    234    234            �
           2604    34155    sensorrepeticao senrepid    DEFAULT     �   ALTER TABLE ONLY public.sensorrepeticao ALTER COLUMN senrepid SET DEFAULT nextval('public.sensorrepeticao_senrepid_seq'::regclass);
 G   ALTER TABLE public.sensorrepeticao ALTER COLUMN senrepid DROP DEFAULT;
       public       postgres    false    228    227    228            �
           2604    34084    tanque tanid    DEFAULT     l   ALTER TABLE ONLY public.tanque ALTER COLUMN tanid SET DEFAULT nextval('public.tanque_tanid_seq'::regclass);
 ;   ALTER TABLE public.tanque ALTER COLUMN tanid DROP DEFAULT;
       public       postgres    false    219    220    220            �
           2604    33987    telefones telid    DEFAULT     r   ALTER TABLE ONLY public.telefones ALTER COLUMN telid SET DEFAULT nextval('public.telefones_telid_seq'::regclass);
 >   ALTER TABLE public.telefones ALTER COLUMN telid DROP DEFAULT;
       public       postgres    false    201    202    202            �
           2604    34034    tipoatuador tipatuid    DEFAULT     |   ALTER TABLE ONLY public.tipoatuador ALTER COLUMN tipatuid SET DEFAULT nextval('public.tipoatuador_tipatuid_seq'::regclass);
 C   ALTER TABLE public.tipoatuador ALTER COLUMN tipatuid DROP DEFAULT;
       public       postgres    false    209    210    210            �
           2604    34042    tiporepeticao tiprepid    DEFAULT     �   ALTER TABLE ONLY public.tiporepeticao ALTER COLUMN tiprepid SET DEFAULT nextval('public.tiporepeticao_tiprepid_seq'::regclass);
 E   ALTER TABLE public.tiporepeticao ALTER COLUMN tiprepid DROP DEFAULT;
       public       postgres    false    211    212    212            �
           2604    34026    tiposensor tipsenid    DEFAULT     z   ALTER TABLE ONLY public.tiposensor ALTER COLUMN tipsenid SET DEFAULT nextval('public.tiposensor_tipsenid_seq'::regclass);
 B   ALTER TABLE public.tiposensor ALTER COLUMN tipsenid DROP DEFAULT;
       public       postgres    false    208    207    208            �
           2604    34008    usuarios usuid    DEFAULT     p   ALTER TABLE ONLY public.usuarios ALTER COLUMN usuid SET DEFAULT nextval('public.usuarios_usuid_seq'::regclass);
 =   ALTER TABLE public.usuarios ALTER COLUMN usuid DROP DEFAULT;
       public       postgres    false    205    206    206            �          0    34095    arduino 
   TABLE DATA               \   COPY public.arduino (ardid, ardlocal, ardstatus, ardvirtual, ardativo, fktanid) FROM stdin;
    public       postgres    false    222   ��       �          0    34188    arduinoatuador 
   TABLE DATA               D   COPY public.arduinoatuador (ardsenid, fkardid, fkatuid) FROM stdin;
    public       postgres    false    232   ��       �          0    34224    arduinocontrole 
   TABLE DATA               F   COPY public.arduinocontrole (ardcontid, fkardid, fkconid) FROM stdin;
    public       postgres    false    236   �       �          0    34170    arduinosensor 
   TABLE DATA               C   COPY public.arduinosensor (ardsenid, fkardid, fksenid) FROM stdin;
    public       postgres    false    230   �       �          0    34060    atuador 
   TABLE DATA               v   COPY public.atuador (atuid, atudescricao, atuparametro, atuatualizacao, atuativo, atuvirtual, fktipatuid) FROM stdin;
    public       postgres    false    216   ;�       �          0    34108    controle 
   TABLE DATA               5   COPY public.controle (conid, concriacao) FROM stdin;
    public       postgres    false    224   X�       �          0    34242    controlegrpacesso 
   TABLE DATA               G   COPY public.controlegrpacesso (congrpid, fkconid, fkgrpid) FROM stdin;
    public       postgres    false    238   u�       �          0    33970    criacao 
   TABLE DATA               @   COPY public.criacao (criaid, criaespecie, criadata) FROM stdin;
    public       postgres    false    199   ��       �          0    33962    empresa 
   TABLE DATA               D   COPY public.empresa (empid, empnome, empcnpj, emplocal) FROM stdin;
    public       postgres    false    197   ��       �          0    33977    funcionario 
   TABLE DATA               W   COPY public.funcionario (funmatricula, funnome, funcpf, funsexo, funativo) FROM stdin;
    public       postgres    false    200   ��       �          0    33997 	   grpacesso 
   TABLE DATA               B   COPY public.grpacesso (grpid, grpdescricao, grpativo) FROM stdin;
    public       postgres    false    204   ��       �          0    34117 	   historico 
   TABLE DATA               k   COPY public.historico (histid, fkardid, fktanid, fksenid, fkatuid, fkempid, hora, dia, medida) FROM stdin;
    public       postgres    false    226   �       �          0    34073    horarepeticao 
   TABLE DATA               i   COPY public.horarepeticao (horid, horhora, hordiainicial, horrepetiracada, hortiporepeticao) FROM stdin;
    public       postgres    false    218   #�       �          0    34047    sensor 
   TABLE DATA               u   COPY public.sensor (senid, sendescricao, senparametro, senatualizacao, senativo, senvirtual, fktipsenid) FROM stdin;
    public       postgres    false    214   @�       �          0    34206    sensorhorarepeticao 
   TABLE DATA               I   COPY public.sensorhorarepeticao (senhorid, fksenid, fkhorid) FROM stdin;
    public       postgres    false    234   ]�       �          0    34152    sensorrepeticao 
   TABLE DATA               H   COPY public.sensorrepeticao (senrepid, fktiprepid, fksenid) FROM stdin;
    public       postgres    false    228   z�       �          0    34081    tanque 
   TABLE DATA               S   COPY public.tanque (tanid, tanidentificacao, tandatainicio, fkcriacao) FROM stdin;
    public       postgres    false    220   ��       �          0    33984 	   telefones 
   TABLE DATA               E   COPY public.telefones (telid, telnumero, fkfunmatricula) FROM stdin;
    public       postgres    false    202   ��       �          0    34031    tipoatuador 
   TABLE DATA               L   COPY public.tipoatuador (tipatuid, tipatunome, tipatudescricao) FROM stdin;
    public       postgres    false    210   ��       �          0    34039    tiporepeticao 
   TABLE DATA               N   COPY public.tiporepeticao (tiprepid, tiprepnome, tiprepdescricao) FROM stdin;
    public       postgres    false    212   ��       �          0    34023 
   tiposensor 
   TABLE DATA               K   COPY public.tiposensor (tipsenid, tipsennome, tipsendescricao) FROM stdin;
    public       postgres    false    208   �       �          0    34005    usuarios 
   TABLE DATA               _   COPY public.usuarios (usuid, usunome, ususenha, usuativo, fkfunmatricula, fkgrpid) FROM stdin;
    public       postgres    false    206   (�       	           0    0    arduino_ardid_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.arduino_ardid_seq', 1, false);
            public       postgres    false    221            
           0    0    arduinoatuador_ardsenid_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.arduinoatuador_ardsenid_seq', 1, false);
            public       postgres    false    231                       0    0    arduinocontrole_ardcontid_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.arduinocontrole_ardcontid_seq', 1, false);
            public       postgres    false    235                       0    0    arduinosensor_ardsenid_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.arduinosensor_ardsenid_seq', 1, false);
            public       postgres    false    229                       0    0    atuador_atuid_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.atuador_atuid_seq', 1, false);
            public       postgres    false    215                       0    0    controle_conid_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.controle_conid_seq', 1, false);
            public       postgres    false    223                       0    0    controlegrpacesso_congrpid_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.controlegrpacesso_congrpid_seq', 1, false);
            public       postgres    false    237                       0    0    criacao_criaid_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.criacao_criaid_seq', 1, false);
            public       postgres    false    198                       0    0    empresa_empid_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.empresa_empid_seq', 1, false);
            public       postgres    false    196                       0    0    grpacesso_grpid_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.grpacesso_grpid_seq', 1, false);
            public       postgres    false    203                       0    0    historico_histid_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.historico_histid_seq', 1, false);
            public       postgres    false    225                       0    0    horarepeticao_horid_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.horarepeticao_horid_seq', 1, false);
            public       postgres    false    217                       0    0    sensor_senid_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.sensor_senid_seq', 1, false);
            public       postgres    false    213                       0    0     sensorhorarepeticao_senhorid_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.sensorhorarepeticao_senhorid_seq', 1, false);
            public       postgres    false    233                       0    0    sensorrepeticao_senrepid_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.sensorrepeticao_senrepid_seq', 1, false);
            public       postgres    false    227                       0    0    tanque_tanid_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.tanque_tanid_seq', 1, false);
            public       postgres    false    219                       0    0    telefones_telid_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.telefones_telid_seq', 1, false);
            public       postgres    false    201                       0    0    tipoatuador_tipatuid_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.tipoatuador_tipatuid_seq', 1, false);
            public       postgres    false    209                       0    0    tiporepeticao_tiprepid_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.tiporepeticao_tiprepid_seq', 1, false);
            public       postgres    false    211                       0    0    tiposensor_tipsenid_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tiposensor_tipsenid_seq', 1, false);
            public       postgres    false    207                       0    0    usuarios_usuid_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.usuarios_usuid_seq', 1, false);
            public       postgres    false    205                       2606    34100    arduino arduino_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.arduino
    ADD CONSTRAINT arduino_pkey PRIMARY KEY (ardid);
 >   ALTER TABLE ONLY public.arduino DROP CONSTRAINT arduino_pkey;
       public         postgres    false    222            )           2606    34193 "   arduinoatuador arduinoatuador_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.arduinoatuador
    ADD CONSTRAINT arduinoatuador_pkey PRIMARY KEY (ardsenid);
 L   ALTER TABLE ONLY public.arduinoatuador DROP CONSTRAINT arduinoatuador_pkey;
       public         postgres    false    232            -           2606    34229 $   arduinocontrole arduinocontrole_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.arduinocontrole
    ADD CONSTRAINT arduinocontrole_pkey PRIMARY KEY (ardcontid);
 N   ALTER TABLE ONLY public.arduinocontrole DROP CONSTRAINT arduinocontrole_pkey;
       public         postgres    false    236            '           2606    34175     arduinosensor arduinosensor_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.arduinosensor
    ADD CONSTRAINT arduinosensor_pkey PRIMARY KEY (ardsenid);
 J   ALTER TABLE ONLY public.arduinosensor DROP CONSTRAINT arduinosensor_pkey;
       public         postgres    false    230                       2606    34065    atuador atuador_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.atuador
    ADD CONSTRAINT atuador_pkey PRIMARY KEY (atuid);
 >   ALTER TABLE ONLY public.atuador DROP CONSTRAINT atuador_pkey;
       public         postgres    false    216            !           2606    34114    controle controle_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.controle
    ADD CONSTRAINT controle_pkey PRIMARY KEY (conid);
 @   ALTER TABLE ONLY public.controle DROP CONSTRAINT controle_pkey;
       public         postgres    false    224            /           2606    34247 (   controlegrpacesso controlegrpacesso_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.controlegrpacesso
    ADD CONSTRAINT controlegrpacesso_pkey PRIMARY KEY (congrpid);
 R   ALTER TABLE ONLY public.controlegrpacesso DROP CONSTRAINT controlegrpacesso_pkey;
       public         postgres    false    238                       2606    33976    criacao criacao_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.criacao
    ADD CONSTRAINT criacao_pkey PRIMARY KEY (criaid);
 >   ALTER TABLE ONLY public.criacao DROP CONSTRAINT criacao_pkey;
       public         postgres    false    199                       2606    33967    empresa empresa_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (empid);
 >   ALTER TABLE ONLY public.empresa DROP CONSTRAINT empresa_pkey;
       public         postgres    false    197            	           2606    33981    funcionario funcionario_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.funcionario
    ADD CONSTRAINT funcionario_pkey PRIMARY KEY (funmatricula);
 F   ALTER TABLE ONLY public.funcionario DROP CONSTRAINT funcionario_pkey;
       public         postgres    false    200                       2606    34002    grpacesso grpacesso_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.grpacesso
    ADD CONSTRAINT grpacesso_pkey PRIMARY KEY (grpid);
 B   ALTER TABLE ONLY public.grpacesso DROP CONSTRAINT grpacesso_pkey;
       public         postgres    false    204            #           2606    34124    historico historico_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_pkey PRIMARY KEY (histid);
 B   ALTER TABLE ONLY public.historico DROP CONSTRAINT historico_pkey;
       public         postgres    false    226                       2606    34078     horarepeticao horarepeticao_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.horarepeticao
    ADD CONSTRAINT horarepeticao_pkey PRIMARY KEY (horid);
 J   ALTER TABLE ONLY public.horarepeticao DROP CONSTRAINT horarepeticao_pkey;
       public         postgres    false    218                       2606    34052    sensor sensor_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_pkey PRIMARY KEY (senid);
 <   ALTER TABLE ONLY public.sensor DROP CONSTRAINT sensor_pkey;
       public         postgres    false    214            +           2606    34211 ,   sensorhorarepeticao sensorhorarepeticao_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.sensorhorarepeticao
    ADD CONSTRAINT sensorhorarepeticao_pkey PRIMARY KEY (senhorid);
 V   ALTER TABLE ONLY public.sensorhorarepeticao DROP CONSTRAINT sensorhorarepeticao_pkey;
       public         postgres    false    234            %           2606    34157 $   sensorrepeticao sensorrepeticao_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.sensorrepeticao
    ADD CONSTRAINT sensorrepeticao_pkey PRIMARY KEY (senrepid);
 N   ALTER TABLE ONLY public.sensorrepeticao DROP CONSTRAINT sensorrepeticao_pkey;
       public         postgres    false    228                       2606    34087    tanque tanque_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.tanque
    ADD CONSTRAINT tanque_pkey PRIMARY KEY (tanid);
 <   ALTER TABLE ONLY public.tanque DROP CONSTRAINT tanque_pkey;
       public         postgres    false    220                       2606    33989    telefones telefones_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.telefones
    ADD CONSTRAINT telefones_pkey PRIMARY KEY (telid);
 B   ALTER TABLE ONLY public.telefones DROP CONSTRAINT telefones_pkey;
       public         postgres    false    202                       2606    34036    tipoatuador tipoatuador_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.tipoatuador
    ADD CONSTRAINT tipoatuador_pkey PRIMARY KEY (tipatuid);
 F   ALTER TABLE ONLY public.tipoatuador DROP CONSTRAINT tipoatuador_pkey;
       public         postgres    false    210                       2606    34044     tiporepeticao tiporepeticao_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tiporepeticao
    ADD CONSTRAINT tiporepeticao_pkey PRIMARY KEY (tiprepid);
 J   ALTER TABLE ONLY public.tiporepeticao DROP CONSTRAINT tiporepeticao_pkey;
       public         postgres    false    212                       2606    34028    tiposensor tiposensor_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tiposensor
    ADD CONSTRAINT tiposensor_pkey PRIMARY KEY (tipsenid);
 D   ALTER TABLE ONLY public.tiposensor DROP CONSTRAINT tiposensor_pkey;
       public         postgres    false    208                       2606    34010    usuarios usuarios_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuid);
 @   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_pkey;
       public         postgres    false    206            6           2606    34101    arduino arduino_fktanid_fkey    FK CONSTRAINT        ALTER TABLE ONLY public.arduino
    ADD CONSTRAINT arduino_fktanid_fkey FOREIGN KEY (fktanid) REFERENCES public.tanque(tanid);
 F   ALTER TABLE ONLY public.arduino DROP CONSTRAINT arduino_fktanid_fkey;
       public       postgres    false    222    220    2845            @           2606    34194 *   arduinoatuador arduinoatuador_fkardid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.arduinoatuador
    ADD CONSTRAINT arduinoatuador_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);
 T   ALTER TABLE ONLY public.arduinoatuador DROP CONSTRAINT arduinoatuador_fkardid_fkey;
       public       postgres    false    232    222    2847            A           2606    34199 *   arduinoatuador arduinoatuador_fkatuid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.arduinoatuador
    ADD CONSTRAINT arduinoatuador_fkatuid_fkey FOREIGN KEY (fkatuid) REFERENCES public.atuador(atuid);
 T   ALTER TABLE ONLY public.arduinoatuador DROP CONSTRAINT arduinoatuador_fkatuid_fkey;
       public       postgres    false    2841    216    232            D           2606    34230 ,   arduinocontrole arduinocontrole_fkardid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.arduinocontrole
    ADD CONSTRAINT arduinocontrole_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);
 V   ALTER TABLE ONLY public.arduinocontrole DROP CONSTRAINT arduinocontrole_fkardid_fkey;
       public       postgres    false    236    2847    222            E           2606    34235 ,   arduinocontrole arduinocontrole_fkconid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.arduinocontrole
    ADD CONSTRAINT arduinocontrole_fkconid_fkey FOREIGN KEY (fkconid) REFERENCES public.controle(conid);
 V   ALTER TABLE ONLY public.arduinocontrole DROP CONSTRAINT arduinocontrole_fkconid_fkey;
       public       postgres    false    2849    224    236            >           2606    34176 (   arduinosensor arduinosensor_fkardid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.arduinosensor
    ADD CONSTRAINT arduinosensor_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);
 R   ALTER TABLE ONLY public.arduinosensor DROP CONSTRAINT arduinosensor_fkardid_fkey;
       public       postgres    false    222    230    2847            ?           2606    34181 (   arduinosensor arduinosensor_fksenid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.arduinosensor
    ADD CONSTRAINT arduinosensor_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);
 R   ALTER TABLE ONLY public.arduinosensor DROP CONSTRAINT arduinosensor_fksenid_fkey;
       public       postgres    false    214    230    2839            4           2606    34066    atuador atuador_fktipatuid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.atuador
    ADD CONSTRAINT atuador_fktipatuid_fkey FOREIGN KEY (fktipatuid) REFERENCES public.tipoatuador(tipatuid);
 I   ALTER TABLE ONLY public.atuador DROP CONSTRAINT atuador_fktipatuid_fkey;
       public       postgres    false    210    2835    216            F           2606    34248 0   controlegrpacesso controlegrpacesso_fkconid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.controlegrpacesso
    ADD CONSTRAINT controlegrpacesso_fkconid_fkey FOREIGN KEY (fkconid) REFERENCES public.controle(conid);
 Z   ALTER TABLE ONLY public.controlegrpacesso DROP CONSTRAINT controlegrpacesso_fkconid_fkey;
       public       postgres    false    2849    224    238            G           2606    34253 0   controlegrpacesso controlegrpacesso_fkgrpid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.controlegrpacesso
    ADD CONSTRAINT controlegrpacesso_fkgrpid_fkey FOREIGN KEY (fkgrpid) REFERENCES public.grpacesso(grpid);
 Z   ALTER TABLE ONLY public.controlegrpacesso DROP CONSTRAINT controlegrpacesso_fkgrpid_fkey;
       public       postgres    false    2829    238    204            7           2606    34125     historico historico_fkardid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);
 J   ALTER TABLE ONLY public.historico DROP CONSTRAINT historico_fkardid_fkey;
       public       postgres    false    2847    222    226            :           2606    34140     historico historico_fkatuid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fkatuid_fkey FOREIGN KEY (fkatuid) REFERENCES public.atuador(atuid);
 J   ALTER TABLE ONLY public.historico DROP CONSTRAINT historico_fkatuid_fkey;
       public       postgres    false    226    216    2841            ;           2606    34145     historico historico_fkempid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fkempid_fkey FOREIGN KEY (fkempid) REFERENCES public.empresa(empid);
 J   ALTER TABLE ONLY public.historico DROP CONSTRAINT historico_fkempid_fkey;
       public       postgres    false    197    2821    226            9           2606    34135     historico historico_fksenid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);
 J   ALTER TABLE ONLY public.historico DROP CONSTRAINT historico_fksenid_fkey;
       public       postgres    false    214    226    2839            8           2606    34130     historico historico_fktanid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fktanid_fkey FOREIGN KEY (fktanid) REFERENCES public.tanque(tanid);
 J   ALTER TABLE ONLY public.historico DROP CONSTRAINT historico_fktanid_fkey;
       public       postgres    false    220    2845    226            3           2606    34053    sensor sensor_fktipsenid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_fktipsenid_fkey FOREIGN KEY (fktipsenid) REFERENCES public.tiposensor(tipsenid);
 G   ALTER TABLE ONLY public.sensor DROP CONSTRAINT sensor_fktipsenid_fkey;
       public       postgres    false    208    2833    214            B           2606    34212 4   sensorhorarepeticao sensorhorarepeticao_fkhorid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensorhorarepeticao
    ADD CONSTRAINT sensorhorarepeticao_fkhorid_fkey FOREIGN KEY (fkhorid) REFERENCES public.horarepeticao(horid);
 ^   ALTER TABLE ONLY public.sensorhorarepeticao DROP CONSTRAINT sensorhorarepeticao_fkhorid_fkey;
       public       postgres    false    2843    234    218            C           2606    34217 4   sensorhorarepeticao sensorhorarepeticao_fksenid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensorhorarepeticao
    ADD CONSTRAINT sensorhorarepeticao_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);
 ^   ALTER TABLE ONLY public.sensorhorarepeticao DROP CONSTRAINT sensorhorarepeticao_fksenid_fkey;
       public       postgres    false    2839    234    214            =           2606    34163 ,   sensorrepeticao sensorrepeticao_fksenid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensorrepeticao
    ADD CONSTRAINT sensorrepeticao_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);
 V   ALTER TABLE ONLY public.sensorrepeticao DROP CONSTRAINT sensorrepeticao_fksenid_fkey;
       public       postgres    false    2839    228    214            <           2606    34158 /   sensorrepeticao sensorrepeticao_fktiprepid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensorrepeticao
    ADD CONSTRAINT sensorrepeticao_fktiprepid_fkey FOREIGN KEY (fktiprepid) REFERENCES public.tiporepeticao(tiprepid);
 Y   ALTER TABLE ONLY public.sensorrepeticao DROP CONSTRAINT sensorrepeticao_fktiprepid_fkey;
       public       postgres    false    228    2837    212            5           2606    34088    tanque tanque_fkcriacao_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tanque
    ADD CONSTRAINT tanque_fkcriacao_fkey FOREIGN KEY (fkcriacao) REFERENCES public.criacao(criaid);
 F   ALTER TABLE ONLY public.tanque DROP CONSTRAINT tanque_fkcriacao_fkey;
       public       postgres    false    2823    220    199            0           2606    33990 '   telefones telefones_fkfunmatricula_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.telefones
    ADD CONSTRAINT telefones_fkfunmatricula_fkey FOREIGN KEY (fkfunmatricula) REFERENCES public.funcionario(funmatricula);
 Q   ALTER TABLE ONLY public.telefones DROP CONSTRAINT telefones_fkfunmatricula_fkey;
       public       postgres    false    200    202    2825            1           2606    34011 %   usuarios usuarios_fkfunmatricula_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_fkfunmatricula_fkey FOREIGN KEY (fkfunmatricula) REFERENCES public.funcionario(funmatricula);
 O   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_fkfunmatricula_fkey;
       public       postgres    false    206    2825    200            2           2606    34016    usuarios usuarios_fkgrpid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_fkgrpid_fkey FOREIGN KEY (fkgrpid) REFERENCES public.grpacesso(grpid);
 H   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_fkgrpid_fkey;
       public       postgres    false    206    2829    204            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �     