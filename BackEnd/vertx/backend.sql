--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tabajara; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE tabajara WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';


ALTER DATABASE tabajara OWNER TO postgres;

\connect tabajara

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: arduino; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arduino (
    ardid integer NOT NULL,
    ardlocal character varying(50),
    ardstatus character varying(100),
    ardvirtual character varying(20),
    ardativo character varying(20),
    fktanid integer
);


ALTER TABLE public.arduino OWNER TO postgres;

--
-- Name: arduino_ardid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arduino_ardid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arduino_ardid_seq OWNER TO postgres;

--
-- Name: arduino_ardid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arduino_ardid_seq OWNED BY public.arduino.ardid;


--
-- Name: arduinoatuador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arduinoatuador (
    ardsenid integer NOT NULL,
    fkardid integer,
    fkatuid integer
);


ALTER TABLE public.arduinoatuador OWNER TO postgres;

--
-- Name: arduinoatuador_ardsenid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arduinoatuador_ardsenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arduinoatuador_ardsenid_seq OWNER TO postgres;

--
-- Name: arduinoatuador_ardsenid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arduinoatuador_ardsenid_seq OWNED BY public.arduinoatuador.ardsenid;


--
-- Name: arduinocontrole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arduinocontrole (
    ardcontid integer NOT NULL,
    fkardid integer,
    fkconid integer
);


ALTER TABLE public.arduinocontrole OWNER TO postgres;

--
-- Name: arduinocontrole_ardcontid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arduinocontrole_ardcontid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arduinocontrole_ardcontid_seq OWNER TO postgres;

--
-- Name: arduinocontrole_ardcontid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arduinocontrole_ardcontid_seq OWNED BY public.arduinocontrole.ardcontid;


--
-- Name: arduinosensor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arduinosensor (
    ardsenid integer NOT NULL,
    fkardid integer,
    fksenid integer
);


ALTER TABLE public.arduinosensor OWNER TO postgres;

--
-- Name: arduinosensor_ardsenid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arduinosensor_ardsenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arduinosensor_ardsenid_seq OWNER TO postgres;

--
-- Name: arduinosensor_ardsenid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arduinosensor_ardsenid_seq OWNED BY public.arduinosensor.ardsenid;


--
-- Name: atuador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.atuador (
    atuid integer NOT NULL,
    atudescricao character varying(50),
    atuparametro character varying(50),
    atuatualizacao integer,
    atuativo character varying(20),
    atuvirtual character varying(20),
    fktipatuid integer
);


ALTER TABLE public.atuador OWNER TO postgres;

--
-- Name: atuador_atuid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.atuador_atuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.atuador_atuid_seq OWNER TO postgres;

--
-- Name: atuador_atuid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.atuador_atuid_seq OWNED BY public.atuador.atuid;


--
-- Name: controle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.controle (
    conid integer NOT NULL,
    concriacao date DEFAULT CURRENT_DATE
);


ALTER TABLE public.controle OWNER TO postgres;

--
-- Name: controle_conid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.controle_conid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.controle_conid_seq OWNER TO postgres;

--
-- Name: controle_conid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.controle_conid_seq OWNED BY public.controle.conid;


--
-- Name: controlegrpacesso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.controlegrpacesso (
    congrpid integer NOT NULL,
    fkconid integer,
    fkgrpid integer
);


ALTER TABLE public.controlegrpacesso OWNER TO postgres;

--
-- Name: controlegrpacesso_congrpid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.controlegrpacesso_congrpid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.controlegrpacesso_congrpid_seq OWNER TO postgres;

--
-- Name: controlegrpacesso_congrpid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.controlegrpacesso_congrpid_seq OWNED BY public.controlegrpacesso.congrpid;


--
-- Name: criacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.criacao (
    criaid integer NOT NULL,
    criaespecie character varying(50),
    criadata date DEFAULT CURRENT_DATE
);


ALTER TABLE public.criacao OWNER TO postgres;

--
-- Name: criacao_criaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.criacao_criaid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.criacao_criaid_seq OWNER TO postgres;

--
-- Name: criacao_criaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.criacao_criaid_seq OWNED BY public.criacao.criaid;


--
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa (
    empid integer NOT NULL,
    empnome character varying(50),
    empcnpj character varying(50),
    emplocal character varying(100)
);


ALTER TABLE public.empresa OWNER TO postgres;

--
-- Name: empresa_empid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.empresa_empid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empresa_empid_seq OWNER TO postgres;

--
-- Name: empresa_empid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.empresa_empid_seq OWNED BY public.empresa.empid;


--
-- Name: funcionario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.funcionario (
    funmatricula character varying(30) NOT NULL,
    funnome character varying(50),
    funcpf character varying(14),
    funsexo character varying(14),
    funativo character varying(14)
);


ALTER TABLE public.funcionario OWNER TO postgres;

--
-- Name: grpacesso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grpacesso (
    grpid integer NOT NULL,
    grpdescricao character varying(50),
    grpativo boolean
);


ALTER TABLE public.grpacesso OWNER TO postgres;

--
-- Name: grpacesso_grpid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grpacesso_grpid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grpacesso_grpid_seq OWNER TO postgres;

--
-- Name: grpacesso_grpid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grpacesso_grpid_seq OWNED BY public.grpacesso.grpid;


--
-- Name: historico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historico (
    histid integer NOT NULL,
    fkardid integer,
    fktanid integer,
    fksenid integer,
    fkatuid integer,
    fkempid integer,
    hora character varying(50) DEFAULT CURRENT_TIME,
    dia date DEFAULT CURRENT_DATE,
    medida character varying(50)
);


ALTER TABLE public.historico OWNER TO postgres;

--
-- Name: historico_histid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historico_histid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historico_histid_seq OWNER TO postgres;

--
-- Name: historico_histid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historico_histid_seq OWNED BY public.historico.histid;


--
-- Name: horarepeticao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.horarepeticao (
    horid integer NOT NULL,
    horhora character varying(5),
    hordiainicial date,
    horrepetiracada integer,
    hortiporepeticao character varying(50)
);


ALTER TABLE public.horarepeticao OWNER TO postgres;

--
-- Name: horarepeticao_horid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.horarepeticao_horid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.horarepeticao_horid_seq OWNER TO postgres;

--
-- Name: horarepeticao_horid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.horarepeticao_horid_seq OWNED BY public.horarepeticao.horid;


--
-- Name: sensor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sensor (
    senid integer NOT NULL,
    sendescricao character varying(50),
    senparametro character varying(50),
    senatualizacao integer,
    senativo character varying(20),
    senvirtual character varying(20),
    fktipsenid integer
);


ALTER TABLE public.sensor OWNER TO postgres;

--
-- Name: sensor_senid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sensor_senid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sensor_senid_seq OWNER TO postgres;

--
-- Name: sensor_senid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sensor_senid_seq OWNED BY public.sensor.senid;


--
-- Name: sensorhorarepeticao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sensorhorarepeticao (
    senhorid integer NOT NULL,
    fksenid integer,
    fkhorid integer
);


ALTER TABLE public.sensorhorarepeticao OWNER TO postgres;

--
-- Name: sensorhorarepeticao_senhorid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sensorhorarepeticao_senhorid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sensorhorarepeticao_senhorid_seq OWNER TO postgres;

--
-- Name: sensorhorarepeticao_senhorid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sensorhorarepeticao_senhorid_seq OWNED BY public.sensorhorarepeticao.senhorid;


--
-- Name: sensorrepeticao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sensorrepeticao (
    senrepid integer NOT NULL,
    fktiprepid integer,
    fksenid integer
);


ALTER TABLE public.sensorrepeticao OWNER TO postgres;

--
-- Name: sensorrepeticao_senrepid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sensorrepeticao_senrepid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sensorrepeticao_senrepid_seq OWNER TO postgres;

--
-- Name: sensorrepeticao_senrepid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sensorrepeticao_senrepid_seq OWNED BY public.sensorrepeticao.senrepid;


--
-- Name: tanque; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tanque (
    tanid integer NOT NULL,
    tanidentificacao character varying(50),
    tandatainicio date DEFAULT CURRENT_DATE,
    fkcriacao integer
);


ALTER TABLE public.tanque OWNER TO postgres;

--
-- Name: tanque_tanid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tanque_tanid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tanque_tanid_seq OWNER TO postgres;

--
-- Name: tanque_tanid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tanque_tanid_seq OWNED BY public.tanque.tanid;


--
-- Name: telefones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.telefones (
    telid integer NOT NULL,
    telnumero character varying(11),
    fkfunmatricula character varying(30)
);


ALTER TABLE public.telefones OWNER TO postgres;

--
-- Name: telefones_telid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.telefones_telid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telefones_telid_seq OWNER TO postgres;

--
-- Name: telefones_telid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.telefones_telid_seq OWNED BY public.telefones.telid;


--
-- Name: tipoatuador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipoatuador (
    tipatuid integer NOT NULL,
    tipatunome character varying(50),
    tipatudescricao character varying(50)
);


ALTER TABLE public.tipoatuador OWNER TO postgres;

--
-- Name: tipoatuador_tipatuid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipoatuador_tipatuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoatuador_tipatuid_seq OWNER TO postgres;

--
-- Name: tipoatuador_tipatuid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipoatuador_tipatuid_seq OWNED BY public.tipoatuador.tipatuid;


--
-- Name: tiporepeticao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tiporepeticao (
    tiprepid integer NOT NULL,
    tiprepnome character varying(50),
    tiprepdescricao character varying(50)
);


ALTER TABLE public.tiporepeticao OWNER TO postgres;

--
-- Name: tiporepeticao_tiprepid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tiporepeticao_tiprepid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiporepeticao_tiprepid_seq OWNER TO postgres;

--
-- Name: tiporepeticao_tiprepid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tiporepeticao_tiprepid_seq OWNED BY public.tiporepeticao.tiprepid;


--
-- Name: tiposensor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tiposensor (
    tipsenid integer NOT NULL,
    tipsennome character varying(50),
    tipsendescricao character varying(50)
);


ALTER TABLE public.tiposensor OWNER TO postgres;

--
-- Name: tiposensor_tipsenid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tiposensor_tipsenid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiposensor_tipsenid_seq OWNER TO postgres;

--
-- Name: tiposensor_tipsenid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tiposensor_tipsenid_seq OWNED BY public.tiposensor.tipsenid;


--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    usuid integer NOT NULL,
    usunome character varying(20),
    ususenha character varying(20),
    usuativo character varying(20),
    fkfunmatricula character varying(30),
    fkgrpid integer
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- Name: usuarios_usuid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_usuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_usuid_seq OWNER TO postgres;

--
-- Name: usuarios_usuid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_usuid_seq OWNED BY public.usuarios.usuid;


--
-- Name: arduino ardid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduino ALTER COLUMN ardid SET DEFAULT nextval('public.arduino_ardid_seq'::regclass);


--
-- Name: arduinoatuador ardsenid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinoatuador ALTER COLUMN ardsenid SET DEFAULT nextval('public.arduinoatuador_ardsenid_seq'::regclass);


--
-- Name: arduinocontrole ardcontid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinocontrole ALTER COLUMN ardcontid SET DEFAULT nextval('public.arduinocontrole_ardcontid_seq'::regclass);


--
-- Name: arduinosensor ardsenid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinosensor ALTER COLUMN ardsenid SET DEFAULT nextval('public.arduinosensor_ardsenid_seq'::regclass);


--
-- Name: atuador atuid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.atuador ALTER COLUMN atuid SET DEFAULT nextval('public.atuador_atuid_seq'::regclass);


--
-- Name: controle conid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.controle ALTER COLUMN conid SET DEFAULT nextval('public.controle_conid_seq'::regclass);


--
-- Name: controlegrpacesso congrpid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.controlegrpacesso ALTER COLUMN congrpid SET DEFAULT nextval('public.controlegrpacesso_congrpid_seq'::regclass);


--
-- Name: criacao criaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.criacao ALTER COLUMN criaid SET DEFAULT nextval('public.criacao_criaid_seq'::regclass);


--
-- Name: empresa empid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa ALTER COLUMN empid SET DEFAULT nextval('public.empresa_empid_seq'::regclass);


--
-- Name: grpacesso grpid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grpacesso ALTER COLUMN grpid SET DEFAULT nextval('public.grpacesso_grpid_seq'::regclass);


--
-- Name: historico histid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico ALTER COLUMN histid SET DEFAULT nextval('public.historico_histid_seq'::regclass);


--
-- Name: horarepeticao horid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.horarepeticao ALTER COLUMN horid SET DEFAULT nextval('public.horarepeticao_horid_seq'::regclass);


--
-- Name: sensor senid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor ALTER COLUMN senid SET DEFAULT nextval('public.sensor_senid_seq'::regclass);


--
-- Name: sensorhorarepeticao senhorid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorhorarepeticao ALTER COLUMN senhorid SET DEFAULT nextval('public.sensorhorarepeticao_senhorid_seq'::regclass);


--
-- Name: sensorrepeticao senrepid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorrepeticao ALTER COLUMN senrepid SET DEFAULT nextval('public.sensorrepeticao_senrepid_seq'::regclass);


--
-- Name: tanque tanid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanque ALTER COLUMN tanid SET DEFAULT nextval('public.tanque_tanid_seq'::regclass);


--
-- Name: telefones telid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefones ALTER COLUMN telid SET DEFAULT nextval('public.telefones_telid_seq'::regclass);


--
-- Name: tipoatuador tipatuid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipoatuador ALTER COLUMN tipatuid SET DEFAULT nextval('public.tipoatuador_tipatuid_seq'::regclass);


--
-- Name: tiporepeticao tiprepid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiporepeticao ALTER COLUMN tiprepid SET DEFAULT nextval('public.tiporepeticao_tiprepid_seq'::regclass);


--
-- Name: tiposensor tipsenid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiposensor ALTER COLUMN tipsenid SET DEFAULT nextval('public.tiposensor_tipsenid_seq'::regclass);


--
-- Name: usuarios usuid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN usuid SET DEFAULT nextval('public.usuarios_usuid_seq'::regclass);


--
-- Data for Name: arduino; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arduino (ardid, ardlocal, ardstatus, ardvirtual, ardativo, fktanid) FROM stdin;
\.


--
-- Data for Name: arduinoatuador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arduinoatuador (ardsenid, fkardid, fkatuid) FROM stdin;
\.


--
-- Data for Name: arduinocontrole; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arduinocontrole (ardcontid, fkardid, fkconid) FROM stdin;
\.


--
-- Data for Name: arduinosensor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arduinosensor (ardsenid, fkardid, fksenid) FROM stdin;
\.


--
-- Data for Name: atuador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.atuador (atuid, atudescricao, atuparametro, atuatualizacao, atuativo, atuvirtual, fktipatuid) FROM stdin;
\.


--
-- Data for Name: controle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.controle (conid, concriacao) FROM stdin;
\.


--
-- Data for Name: controlegrpacesso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.controlegrpacesso (congrpid, fkconid, fkgrpid) FROM stdin;
\.


--
-- Data for Name: criacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.criacao (criaid, criaespecie, criadata) FROM stdin;
\.


--
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa (empid, empnome, empcnpj, emplocal) FROM stdin;
\.


--
-- Data for Name: funcionario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.funcionario (funmatricula, funnome, funcpf, funsexo, funativo) FROM stdin;
\.


--
-- Data for Name: grpacesso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grpacesso (grpid, grpdescricao, grpativo) FROM stdin;
\.


--
-- Data for Name: historico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historico (histid, fkardid, fktanid, fksenid, fkatuid, fkempid, hora, dia, medida) FROM stdin;
\.


--
-- Data for Name: horarepeticao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.horarepeticao (horid, horhora, hordiainicial, horrepetiracada, hortiporepeticao) FROM stdin;
\.


--
-- Data for Name: sensor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensor (senid, sendescricao, senparametro, senatualizacao, senativo, senvirtual, fktipsenid) FROM stdin;
\.


--
-- Data for Name: sensorhorarepeticao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensorhorarepeticao (senhorid, fksenid, fkhorid) FROM stdin;
\.


--
-- Data for Name: sensorrepeticao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensorrepeticao (senrepid, fktiprepid, fksenid) FROM stdin;
\.


--
-- Data for Name: tanque; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tanque (tanid, tanidentificacao, tandatainicio, fkcriacao) FROM stdin;
\.


--
-- Data for Name: telefones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.telefones (telid, telnumero, fkfunmatricula) FROM stdin;
\.


--
-- Data for Name: tipoatuador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipoatuador (tipatuid, tipatunome, tipatudescricao) FROM stdin;
\.


--
-- Data for Name: tiporepeticao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tiporepeticao (tiprepid, tiprepnome, tiprepdescricao) FROM stdin;
\.


--
-- Data for Name: tiposensor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tiposensor (tipsenid, tipsennome, tipsendescricao) FROM stdin;
\.


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios (usuid, usunome, ususenha, usuativo, fkfunmatricula, fkgrpid) FROM stdin;
\.


--
-- Name: arduino_ardid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arduino_ardid_seq', 1, false);


--
-- Name: arduinoatuador_ardsenid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arduinoatuador_ardsenid_seq', 1, false);


--
-- Name: arduinocontrole_ardcontid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arduinocontrole_ardcontid_seq', 1, false);


--
-- Name: arduinosensor_ardsenid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arduinosensor_ardsenid_seq', 1, false);


--
-- Name: atuador_atuid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.atuador_atuid_seq', 1, false);


--
-- Name: controle_conid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.controle_conid_seq', 1, false);


--
-- Name: controlegrpacesso_congrpid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.controlegrpacesso_congrpid_seq', 1, false);


--
-- Name: criacao_criaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.criacao_criaid_seq', 1, false);


--
-- Name: empresa_empid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.empresa_empid_seq', 1, false);


--
-- Name: grpacesso_grpid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grpacesso_grpid_seq', 1, false);


--
-- Name: historico_histid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historico_histid_seq', 1, false);


--
-- Name: horarepeticao_horid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.horarepeticao_horid_seq', 1, false);


--
-- Name: sensor_senid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sensor_senid_seq', 1, false);


--
-- Name: sensorhorarepeticao_senhorid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sensorhorarepeticao_senhorid_seq', 1, false);


--
-- Name: sensorrepeticao_senrepid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sensorrepeticao_senrepid_seq', 1, false);


--
-- Name: tanque_tanid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tanque_tanid_seq', 1, false);


--
-- Name: telefones_telid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.telefones_telid_seq', 1, false);


--
-- Name: tipoatuador_tipatuid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipoatuador_tipatuid_seq', 1, false);


--
-- Name: tiporepeticao_tiprepid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tiporepeticao_tiprepid_seq', 1, false);


--
-- Name: tiposensor_tipsenid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tiposensor_tipsenid_seq', 1, false);


--
-- Name: usuarios_usuid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_usuid_seq', 1, false);


--
-- Name: arduino arduino_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduino
    ADD CONSTRAINT arduino_pkey PRIMARY KEY (ardid);


--
-- Name: arduinoatuador arduinoatuador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinoatuador
    ADD CONSTRAINT arduinoatuador_pkey PRIMARY KEY (ardsenid);


--
-- Name: arduinocontrole arduinocontrole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinocontrole
    ADD CONSTRAINT arduinocontrole_pkey PRIMARY KEY (ardcontid);


--
-- Name: arduinosensor arduinosensor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinosensor
    ADD CONSTRAINT arduinosensor_pkey PRIMARY KEY (ardsenid);


--
-- Name: atuador atuador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.atuador
    ADD CONSTRAINT atuador_pkey PRIMARY KEY (atuid);


--
-- Name: controle controle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.controle
    ADD CONSTRAINT controle_pkey PRIMARY KEY (conid);


--
-- Name: controlegrpacesso controlegrpacesso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.controlegrpacesso
    ADD CONSTRAINT controlegrpacesso_pkey PRIMARY KEY (congrpid);


--
-- Name: criacao criacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.criacao
    ADD CONSTRAINT criacao_pkey PRIMARY KEY (criaid);


--
-- Name: empresa empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (empid);


--
-- Name: funcionario funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.funcionario
    ADD CONSTRAINT funcionario_pkey PRIMARY KEY (funmatricula);


--
-- Name: grpacesso grpacesso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grpacesso
    ADD CONSTRAINT grpacesso_pkey PRIMARY KEY (grpid);


--
-- Name: historico historico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_pkey PRIMARY KEY (histid);


--
-- Name: horarepeticao horarepeticao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.horarepeticao
    ADD CONSTRAINT horarepeticao_pkey PRIMARY KEY (horid);


--
-- Name: sensor sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_pkey PRIMARY KEY (senid);


--
-- Name: sensorhorarepeticao sensorhorarepeticao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorhorarepeticao
    ADD CONSTRAINT sensorhorarepeticao_pkey PRIMARY KEY (senhorid);


--
-- Name: sensorrepeticao sensorrepeticao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorrepeticao
    ADD CONSTRAINT sensorrepeticao_pkey PRIMARY KEY (senrepid);


--
-- Name: tanque tanque_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanque
    ADD CONSTRAINT tanque_pkey PRIMARY KEY (tanid);


--
-- Name: telefones telefones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefones
    ADD CONSTRAINT telefones_pkey PRIMARY KEY (telid);


--
-- Name: tipoatuador tipoatuador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipoatuador
    ADD CONSTRAINT tipoatuador_pkey PRIMARY KEY (tipatuid);


--
-- Name: tiporepeticao tiporepeticao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiporepeticao
    ADD CONSTRAINT tiporepeticao_pkey PRIMARY KEY (tiprepid);


--
-- Name: tiposensor tiposensor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tiposensor
    ADD CONSTRAINT tiposensor_pkey PRIMARY KEY (tipsenid);


--
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuid);


--
-- Name: arduino arduino_fktanid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduino
    ADD CONSTRAINT arduino_fktanid_fkey FOREIGN KEY (fktanid) REFERENCES public.tanque(tanid);


--
-- Name: arduinoatuador arduinoatuador_fkardid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinoatuador
    ADD CONSTRAINT arduinoatuador_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);


--
-- Name: arduinoatuador arduinoatuador_fkatuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinoatuador
    ADD CONSTRAINT arduinoatuador_fkatuid_fkey FOREIGN KEY (fkatuid) REFERENCES public.atuador(atuid);


--
-- Name: arduinocontrole arduinocontrole_fkardid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinocontrole
    ADD CONSTRAINT arduinocontrole_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);


--
-- Name: arduinocontrole arduinocontrole_fkconid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinocontrole
    ADD CONSTRAINT arduinocontrole_fkconid_fkey FOREIGN KEY (fkconid) REFERENCES public.controle(conid);


--
-- Name: arduinosensor arduinosensor_fkardid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinosensor
    ADD CONSTRAINT arduinosensor_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);


--
-- Name: arduinosensor arduinosensor_fksenid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arduinosensor
    ADD CONSTRAINT arduinosensor_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);


--
-- Name: atuador atuador_fktipatuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.atuador
    ADD CONSTRAINT atuador_fktipatuid_fkey FOREIGN KEY (fktipatuid) REFERENCES public.tipoatuador(tipatuid);


--
-- Name: controlegrpacesso controlegrpacesso_fkconid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.controlegrpacesso
    ADD CONSTRAINT controlegrpacesso_fkconid_fkey FOREIGN KEY (fkconid) REFERENCES public.controle(conid);


--
-- Name: controlegrpacesso controlegrpacesso_fkgrpid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.controlegrpacesso
    ADD CONSTRAINT controlegrpacesso_fkgrpid_fkey FOREIGN KEY (fkgrpid) REFERENCES public.grpacesso(grpid);


--
-- Name: historico historico_fkardid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fkardid_fkey FOREIGN KEY (fkardid) REFERENCES public.arduino(ardid);


--
-- Name: historico historico_fkatuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fkatuid_fkey FOREIGN KEY (fkatuid) REFERENCES public.atuador(atuid);


--
-- Name: historico historico_fkempid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fkempid_fkey FOREIGN KEY (fkempid) REFERENCES public.empresa(empid);


--
-- Name: historico historico_fksenid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);


--
-- Name: historico historico_fktanid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historico
    ADD CONSTRAINT historico_fktanid_fkey FOREIGN KEY (fktanid) REFERENCES public.tanque(tanid);


--
-- Name: sensor sensor_fktipsenid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_fktipsenid_fkey FOREIGN KEY (fktipsenid) REFERENCES public.tiposensor(tipsenid);


--
-- Name: sensorhorarepeticao sensorhorarepeticao_fkhorid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorhorarepeticao
    ADD CONSTRAINT sensorhorarepeticao_fkhorid_fkey FOREIGN KEY (fkhorid) REFERENCES public.horarepeticao(horid);


--
-- Name: sensorhorarepeticao sensorhorarepeticao_fksenid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorhorarepeticao
    ADD CONSTRAINT sensorhorarepeticao_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);


--
-- Name: sensorrepeticao sensorrepeticao_fksenid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorrepeticao
    ADD CONSTRAINT sensorrepeticao_fksenid_fkey FOREIGN KEY (fksenid) REFERENCES public.sensor(senid);


--
-- Name: sensorrepeticao sensorrepeticao_fktiprepid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensorrepeticao
    ADD CONSTRAINT sensorrepeticao_fktiprepid_fkey FOREIGN KEY (fktiprepid) REFERENCES public.tiporepeticao(tiprepid);


--
-- Name: tanque tanque_fkcriacao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanque
    ADD CONSTRAINT tanque_fkcriacao_fkey FOREIGN KEY (fkcriacao) REFERENCES public.criacao(criaid);


--
-- Name: telefones telefones_fkfunmatricula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefones
    ADD CONSTRAINT telefones_fkfunmatricula_fkey FOREIGN KEY (fkfunmatricula) REFERENCES public.funcionario(funmatricula);


--
-- Name: usuarios usuarios_fkfunmatricula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_fkfunmatricula_fkey FOREIGN KEY (fkfunmatricula) REFERENCES public.funcionario(funmatricula);


--
-- Name: usuarios usuarios_fkgrpid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_fkgrpid_fkey FOREIGN KEY (fkgrpid) REFERENCES public.grpacesso(grpid);


--
-- PostgreSQL database dump complete
--

