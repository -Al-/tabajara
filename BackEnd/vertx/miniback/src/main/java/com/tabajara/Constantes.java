package com.tabajara;

public class Constantes {
	Constantes() {
	}

	// arduino
	public static final String atu_GET = "/atuador/:id";
	public static final String atu_LIST_ALL = "/atuador";
	public static final String atu_CREATE = "/atuador";
	public static final String atu_UPDATE = "/atuador/:id";
	public static final String atu_DELETE = "/atuador/:id";
	public static final String atu_DELETE_ALL = "/atuador";
}
