import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuthModule} from 'angularfire2/auth';
import { AngularFireModule} from 'angularfire2';

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { HomePage } from '../pages/home/home';

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyB0D3wRjnlLutzLlsMT_dMY3mLDHWLUKQQ",
    authDomain: "tabajara-c152a.firebaseapp.com",
    databaseURL: "https://tabajara-c152a.firebaseio.com",
    projectId: "tabajara-c152a",
    storageBucket: "tabajara-c152a.appspot.com",
    messagingSenderId: "813852289559"
  };  

@NgModule({
  declarations: [
    MyApp,
    HomePage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider
  ]
})
export class AppModule {}
