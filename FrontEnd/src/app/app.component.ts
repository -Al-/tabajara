import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icone}>;

  constructor(

    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public authService: AuthServiceProvider,
    afAuth: AngularFireAuth) {

    this.initializeApp();

    const authObserver = afAuth.authState.subscribe(user => {
      if (user) {
        this.rootPage = 'LoginPage';
        authObserver.unsubscribe();
      }
      else {
        this.rootPage = HomePage;
        authObserver.unsubscribe();
      }
    })

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Principal', component: HomePage, icone: 'md-home' },
      { title: 'Meus Dados', component: 'MeusDadosPage', icone: 'md-list-box' },
      { title: 'Gerenciar Usuários', component: 'GerenciarUsuariosPage', icone: 'md-settings' },
      { title: 'Sair', component: '', icone: 'md-exit' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page: { title: string, component: string }) {
    switch (page.title) {
      case 'Sair':
        this.nav.setRoot('LoginPage');
        break;
      default:
        this.nav.setRoot(page.component);
    }

    /*openPage(page:{title:string,component:string}) {      
      this.authService.signOut()
      .then(() => {
        this.navCtrl.setRoot(LoginPage);
      })
      .catch((error) => {
        console.error(error);
      });
    }*/
  }
}