import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { User } from '../../providers/auth-service/user';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-criar-conta',
  templateUrl: 'criar-conta.html',
})
export class CriarContaPage {

  user: User = new User();
  @ViewChild('form') form: NgForm;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private toastCtrl: ToastController,
    private authService: AuthServiceProvider,
    public menu: MenuController) {
  }

  ionViewWillEnter(){
    this.menu.swipeEnable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarContaPage');
  }

  doRefresh(refresher) {   
    setTimeout(() => {      
      refresher.complete();
    }, 1500);
  }

  createAccount() {
    if (this.form.form.valid) {
      let toast = this.toastCtrl.create({ 
        message: 'Usuário criado com sucesso!',
        duration: 2000, 
        position: 'bottom' });

      this.authService.createUser(this.user)
        .then((user: any) => {  
          user.sendEmailVerification();
          
          this.navCtrl.setRoot('LoginPage');
          toast.present();
      
          
        })
        .catch((error: any) => {
          if (error.code  == 'auth/email-already-in-use') {
            toast.setMessage('O e-mail digitado já está em uso.');
          } else if (error.code  == 'auth/invalid-email') {
            toast.setMessage('O e-mail digitado não é valido.');
          } else if (error.code  == 'auth/operation-not-allowed') {
            toast.setMessage('Não está habilitado criar usuários.');
          } else if (error.code  == 'auth/weak-password') {
            toast.setMessage('A senha digitada é muito fraca.');
          }
          toast.present();   
          this.navCtrl.setRoot('LoginPage');
      });
    }
  }

}
