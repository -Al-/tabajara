import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GerenciarFuncionariosPage } from './gerenciar-func';

@NgModule({
  declarations: [
    GerenciarFuncionariosPage,
  ],
  imports: [
    IonicPageModule.forChild(GerenciarFuncionariosPage),
  ],
})
export class GerenciarFuncionariosPageModule {}
