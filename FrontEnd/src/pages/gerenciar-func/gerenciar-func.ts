import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ActionSheetController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-gerenciar-func',
  templateUrl: 'gerenciar-func.html',
})
export class GerenciarFuncionariosPage {

  [x: string]: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public actionSheetCtrl: ActionSheetController) {
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GerenciarFuncionariosPage');
  }

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
    }, 1500);
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({      
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Excluir',
          role: 'destructive',          
          handler: () => {
            console.log('Delete clicked');
          }
        }, {
          text: 'Editar',
          handler: () => {
            console.log('Archive clicked');
          }
        }        
      ]
    });
    actionSheet.present();
  }

  insertFunc(){
    this.navCtrl.push('CadastrarFuncPage');
  }
}
