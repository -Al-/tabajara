import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrarFuncPage } from './cadastrar-func';

@NgModule({
  declarations: [
    CadastrarFuncPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastrarFuncPage),
  ],
})
export class CadastrarFuncPageModule {}
