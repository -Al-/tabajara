import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-cadastrar-func',
  templateUrl: 'cadastrar-func.html',
})
export class CadastrarFuncPage {

  formGroup: FormGroup;


  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastrarFuncPage');
  }

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,   
    public alertCtrl: AlertController,    
    public toastCtrl: ToastController
  ) {

    this.formGroup = this.formBuilder.group({
      matricula: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      nome: [],
      cpf: [],
      sexo: [],
      tel: [],
      ativo: []      
    });
  }
}
