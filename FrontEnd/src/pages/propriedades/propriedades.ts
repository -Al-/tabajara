import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { min } from 'rxjs/operator/min';
import { max } from 'rxjs/operator/max';

@IonicPage()
@Component({
  selector: 'page-propriedades',
  templateUrl: 'propriedades.html',
})
export class PropriedadesPage {

  doRefresh(refresher) {
    setTimeout(() => {
      refresher.complete();
    }, 1500);
  }

  //alcalinidade
  @ViewChild('barCanvasAlc') barCanvasAlc;
  barChartAlc: any;
  doce: any;
  salobra: any;
  pAlcVez: boolean = true;

  //amonia
  @ViewChild('barCanvasAmonia') barCanvasAmonia;
  barChartAmonia: any;
  amonia: any;
  pAmoniaVez: boolean = true;

 //dureza total
 @ViewChild('barCanvasDur') barCanvasDur;
 barChartDur: any;
 doceDur: any;
 salobraDur: any;
 pDurezaVez: boolean = true;

  //H2S
  @ViewChild('barCanvasH2S') barCanvasH2S;
  barChartH2S: any;
  h1: any;
  pH2SVez: boolean = true;

  //ph
  @ViewChild('barCanvasPh') barCanvasPh;
  barChartPh: any;
  acida: number;
  neutra: number;
  alcalina: number;
  pPhVez: boolean = true;

  //nivel
  @ViewChild('barCanvasNivel') barCanvasNivel;
  barChartNivel: any;
  superficie: number;
  meio: number;
  fundo: number;
  pNivelVez: boolean = true;

  //temperatura
  @ViewChild('barCanvasTemp') barCanvasTemp;
  barChartTemp: any;
  temperaturaAmbiente: number;
  temperaturaMedia: number;
  temperaturaAlta: number;
  pTempVez: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController
  ) {
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewDidLoad() {
    this.redenrizarChartAlc()
    this.redenrizarChartAmonia()
    this.redenrizarChartPh()
    this.redenrizarChartNivel()
    this.redenrizarChartTemp()
  }

  //alcalinidade
  randomDoce(min, max) {
    return (Math.random() * ((180 ? 180 : 100) - (180 ? 100 : 0) + 1) + (180 ? 100 : 0)) | 0;
}

randomSalobra(min, max) {
  return (Math.random() * ((220 ? 220 : 120) - (220 ? 120 : 0) + 1) + (220 ? 120 : 0)) | 0;
}

  redenrizarChartAlc() {

    setTimeout(() => {
      this.doce = this.randomDoce(min, max);
      this.salobra = this.randomSalobra(min, max);      
      this.barChartAlc = new Chart(this.barCanvasAlc.nativeElement, {

        type: 'bar',
        data: {
          labels: ["Água doce", "Água Salobra"],
          datasets: [{
            label: 'Meio do Tanque',
            data: [this.doce, this.salobra],
            backgroundColor: [
              'rgba(2, 9, 132, 0.2)',
              'rgba(54, 162, 135, 0.2)',
              'rgba(25, 206, 86, 0.2)',
              'rgba(725, 92, 194, 0.2)',
              'rgba(53, 10, 25, 0.2)',
              'rgba(25, 15, 64, 0.2)'
            ],
            borderColor: [
              'rgba(205,99,132,1)',
              'rgba(54, 162, 135, 1)',
              'rgba(155, 06, 186, 1)',
              'rgba(015, 192, 102, 1)',
              'rgba(53, 102, 155, 1)',
              'rgba(255, 129, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

      });
      this.redenrizarChartAlc()
    }, this.pAlcVez ? 0 : 3000);
    this.pAlcVez = false;

  }

  //amonia

  randomAmonia(min, max) {
    return (Math.random() * ((1.0 ? 1.0 : 0.10) - (1.0 ? 0.10 : 0) + 1) + (1.0 ? 0.10 : 0)) | 0;
}

  redenrizarChartAmonia() {

    setTimeout(() => {
      this.amonia = this.randomAmonia;    
      this.barChartAmonia = new Chart(this.barCanvasAmonia.nativeElement, {

        type: 'bar',
        data: {
          labels: ["mg/L"],
          datasets: [{
            label: 'Meio do Tanque',
            data: [this.amonia],
            backgroundColor: [
              'rgba(2, 9, 132, 0.2)',
              'rgba(54, 162, 135, 0.2)',
              'rgba(25, 206, 86, 0.2)',
              'rgba(725, 92, 194, 0.2)',
              'rgba(53, 10, 25, 0.2)',
              'rgba(25, 15, 64, 0.2)'
            ],
            borderColor: [
              'rgba(205,99,132,1)',
              'rgba(54, 162, 135, 1)',
              'rgba(155, 06, 186, 1)',
              'rgba(015, 192, 102, 1)',
              'rgba(53, 102, 155, 1)',
              'rgba(255, 129, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

      });
      this.redenrizarChartAlc()
    }, this.pAmoniaVez ? 0 : 3000);
    this.pAmoniaVez = false;

  }

    //Dureza Total
    randomDoceDur(min, max) {
      return (Math.random() * ((250 ? 250 : 100) - (250 ? 100 : 0) + 1) + (250 ? 100 : 0)) | 0;
  }
  
  randomSalobraDur(min, max) {
    return (Math.random() * ((2250 ? 2250 : 1000) - (2250 ? 1000 : 0) + 1) + (2250 ? 1000 : 0)) | 0;
  }
  
    redenrizarChartDur() {
  
      setTimeout(() => {
        this.doceDur = this.randomDoceDur;
        this.salobraDur = this.randomSalobraDur;      
        this.barChartDur = new Chart(this.barCanvasDur.nativeElement, {
  
          type: 'bar',
          data: {
            labels: ["Água doce", "Água Salobra"],
            datasets: [{
              label: 'Meio do Tanque',
              data: [this.doceDur, this.salobraDur],
              backgroundColor: [
                'rgba(2, 9, 132, 0.2)',
                'rgba(54, 162, 135, 0.2)',
                'rgba(25, 206, 86, 0.2)',
                'rgba(725, 92, 194, 0.2)',
                'rgba(53, 10, 25, 0.2)',
                'rgba(25, 15, 64, 0.2)'
              ],
              borderColor: [
                'rgba(205,99,132,1)',
                'rgba(54, 162, 135, 1)',
                'rgba(155, 06, 186, 1)',
                'rgba(015, 192, 102, 1)',
                'rgba(53, 102, 155, 1)',
                'rgba(255, 129, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
  
        });
        this.redenrizarChartDur()
      }, this.pDurezaVez ? 0 : 3000);
      this.pDurezaVez = false;
  
    }


        //Dureza Total
        randomH1(min, max) {
          return (Math.random() * ((0.01 ? 0.01 : 0) - (0.01 ? 0 : 0) + 1) + (0.01 ? 0 : 0)) | 0;
      }
      
        redenrizarChartH2S() {
      
          setTimeout(() => {
            this.h1 = this.randomH1;                 
            this.barChartH2S = new Chart(this.barCanvasH2S.nativeElement, {
      
              type: 'bar',
              data: {
                labels: ["H2S"],
                datasets: [{
                  label: 'Meio do Tanque',
                  data: [this.h1],
                  backgroundColor: [
                    'rgba(2, 9, 132, 0.2)',
                    'rgba(54, 162, 135, 0.2)',
                    'rgba(25, 206, 86, 0.2)',
                    'rgba(725, 92, 194, 0.2)',
                    'rgba(53, 10, 25, 0.2)',
                    'rgba(25, 15, 64, 0.2)'
                  ],
                  borderColor: [
                    'rgba(205,99,132,1)',
                    'rgba(54, 162, 135, 1)',
                    'rgba(155, 06, 186, 1)',
                    'rgba(015, 192, 102, 1)',
                    'rgba(53, 102, 155, 1)',
                    'rgba(255, 129, 64, 1)'
                  ],
                  borderWidth: 1
                }]
              },
              options: {
                scales: {
                  yAxes: [{
                    ticks: {
                      beginAtZero: true
                    }
                  }]
                }
              }
      
            });
            this.redenrizarChartAlc()
          }, this.pH2SVez ? 0 : 3000);
          this.pH2SVez = false;
      
        }
  //ph
  redenrizarChartPh() {

    setTimeout(() => {
      this.acida = 1 + 1;
      this.neutra = 4 + 1;
      this.alcalina = 10 + 1;
      this.barChartPh = new Chart(this.barCanvasPh.nativeElement, {

        type: 'bar',
        data: {
          labels: ["Ácida", "Neutra", "Alcalina"],
          datasets: [{
            label: 'Potencial Hidrogeniônico',
            data: [this.acida, this.neutra, this.alcalina],
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 56, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(253, 102, 255, 0.2)',
              'rgba(255, 159, 62, 0.2)'
            ],
            borderColor: [
              'rgba(255,199,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 02, 255, 1)',
              'rgba(255, 159, 44, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

      });
      this.redenrizarChartPh()
    }, this.pPhVez ? 0 : 3000);
    this.pPhVez = false;

  }

  //nivel
  redenrizarChartNivel() {

    setTimeout(() => {
      this.superficie = 1 + 1;
      this.meio = 4 + 1;
      this.fundo = 10 + 1;
      this.barChartNivel = new Chart(this.barCanvasNivel.nativeElement, {

        type: 'bar',
        data: {
          labels: ["Superfície", "Meio", "Fundo"],
          datasets: [{
            label: 'Profundidade',
            data: [this.superficie, this.meio, this.fundo],
            backgroundColor: [
              'rgba(2, 9, 132, 0.2)',
              'rgba(54, 162, 135, 0.2)',
              'rgba(25, 206, 86, 0.2)',
              'rgba(725, 92, 194, 0.2)',
              'rgba(53, 10, 25, 0.2)',
              'rgba(25, 15, 64, 0.2)'
            ],
            borderColor: [
              'rgba(205,99,132,1)',
              'rgba(54, 162, 135, 1)',
              'rgba(155, 06, 186, 1)',
              'rgba(015, 192, 102, 1)',
              'rgba(53, 102, 155, 1)',
              'rgba(255, 129, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

      });
      this.redenrizarChartNivel()
    }, this.pNivelVez ? 0 : 3000);
    this.pNivelVez = false;

  }

  //Temperatura
  redenrizarChartTemp() {

    setTimeout(() => {
      this.temperaturaAmbiente = 1 + 1;
      this.temperaturaMedia = 4 + 1;
      this.temperaturaAlta = 10 + 1;
      this.barChartTemp = new Chart(this.barCanvasTemp.nativeElement, {

        type: 'bar',
        data: {
          labels: ["Ambiente", "Média", "Alta"],
          datasets: [{
            label: 'Graus Celsius',
            data: [this.temperaturaAmbiente, this.temperaturaMedia, this.temperaturaAlta],
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

      });
      this.redenrizarChartTemp()
    }, this.pTempVez ? 0 : 3000);
    this.pTempVez = false;

  }

}











//https://www.joshmorony.com/adding-responsive-charts-graphs-to-ionic-2-applications/
//https://www.djamware.com/post/598953f880aca768e4d2b12b/creating-beautiful-charts-easily-using-ionic-3-and-angular-4
