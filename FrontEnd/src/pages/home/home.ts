import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  
  constructor(
    public navCtrl: NavController,
    public menu: MenuController) {
  }

  ionViewWillEnter(){
    this.menu.swipeEnable(true);
  }

  chamaPropriedades(){
    this.navCtrl.push('PropriedadesPage');    
  }

  doRefresh(refresher) { 
    //add this.metodo para fazer requisição dos dados  
    setTimeout(() => {      
      refresher.complete();
    }, 1500);
  }

}
