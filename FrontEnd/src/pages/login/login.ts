import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { User } from '../../providers/auth-service/user';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user: User = new User();
  @ViewChild('form') form: NgForm;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthServiceProvider,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public menu: MenuController) {
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  resetPassword() {
    this.navCtrl.push('EsqueciSenhaPage');
  }

  createAccount() {
    this.navCtrl.push('CriarContaPage');
  }

  signIn() {
    let loader = this.presentLoading();
    if (this.form.form.valid) {
      this.authService.signIn(this.user)
        .then(() => {
          
          this.navCtrl.setRoot(HomePage);
          
          loader.dismiss();
        })

        .catch((error: any) => {
          const loading = this.showAlert()
          if (error.code == 'auth/invalid-email') {
            loading.setMessage('O e-mail digitado não é valido.');
          } else if (error.code == 'auth/user-disabled') {
            loading.setMessage('O usuário está desativado.');
          } else if (error.code == 'auth/user-not-found') {
            loading.setMessage('O usuário não foi encontrado.');
          } else if (error.code == 'auth/wrong-password') {
            loading.setMessage('A senha digitada não é valida.');
          }
          loading.present();
          loader.dismiss();
        });
    }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      buttons: ['OK']
    });
    return alert;

  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Por favor aguarde..."
    });
    loader.present();
    return loader;
  }

  presentConfirm(title,message) {
    let alert = this.alertCtrl.create({
      title: title,
      message:message ,
    });
    alert.present();
  }



}
