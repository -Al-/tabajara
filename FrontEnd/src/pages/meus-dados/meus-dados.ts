import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-meus-dados',
  templateUrl: 'meus-dados.html',
})
export class MeusDadosPage {  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public menu: MenuController) {
  }

  ionViewWillEnter(){
    this.menu.swipeEnable(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MeusDadosPage');
  }

  doRefresh(refresher) {   
    setTimeout(() => {      
      refresher.complete();
    }, 1500);
  }

}
