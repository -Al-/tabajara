Projeto Camarão - Grupo **Tabajara**

**Observações:**
Nosso cliente, a empresa **Camarão do Nordeste**, tem como foco de manter a agua dos camarões em constante monitoramento para evitar o prejuizo com a morte dos mesmo.
O objetivo desse projeto é de forma pratica e barata, o maximo possivel, garantir uma boa qualidade da agua analisando:
	
	Parametro			Frequencia		Horario					Onde medir				Faixa ideal (limites)
	Salinidade			Diária			06:00, 16:00, 22:00		Superficie e fundo		20 a 25 ppt (0,5 a 40 ppt)
	Temperatura			Diária			06:00, 16:00, 22:00		Meia agua				31 a 32 ºC  (26 a 33 ºC)
	Transparência		Diária			13:00					Superficie 				30 a 35 cm	(20 a 40 cm)
	Materia organica	Semanal									fundo	
	Oxigênio dissolvido	Diária			05:00, 08:00, 11:00,	Meia agua				> 5,50 mg/L (4,0 a 6,5 mg/L)
										14:00, 17:00, 20:00, 
										23:00, 02:00	
	
	pH					Diária			06:00, 17:00			Meia agua				7,8 a 8,3   (7,5 a 8,8)
	Alcalinidade 		Semanal 		07:00					Meia agua				Agua doce: 100 a 180
																						Agua salobra: 120 a 220
	Dureza total		Semanal 		07:00					Meia agua				Agua doce: 100 a 250
																						Agua salobra: 1000 a 2.250
	Amonia total (NH3)	Diário  		17:00					Meia agua				< 0,10 mg/L  (0,10 a 1,0 mg/L)
	Nitrito (NO2)		Diário  		17:00					Meia agua				Agua doce: 0,0 a 0,50 
																						Agua salobra: 0,1 a 6,0
	Nitrato				Diário  		17:00					Meia agua				Agua doce: 0,0 a 5,0
																						Agua salobra: 0,0 a 10,0
	H2S					Semanal 		17:00					Meia agua				< 0,01 mg/L
	Silicato			Semanal 		17:00					Meia agua				> 1,0 mg/L

Onde iremos tentar resolver ao maximo (**Temperatura**,**Transparência**,**Materia organica**,**pH**) , por causa do valor dos sensores. Atualmente dentro de Documentação existem algumas informações para auxiliar o desenvolvimento deste projeto.
A necessidade do cliente é que tenha uma visão para o técnico local e uma visão para o administrador do sistema.
Para ligar o Java com o Arduino vamos utilizar o RXTX - Comunicação entre Arduino e Java
